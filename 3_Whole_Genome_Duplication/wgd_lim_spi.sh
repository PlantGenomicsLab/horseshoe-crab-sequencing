#!/bin/bash
#SBATCH --job-name=wgd-hsc
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G
#SBATCH --mail-user=vidya.vuruputoor@uconn.edu
#SBATCH -e %x_%j.err
#SBATCH -o %x_%j.out

module load diamond
module load paml/4.9
module load blast
module load mcl
module load mafft
module load fasttree
module load gcc/8.4.0

source ~/wgd2_2.0.29/wgd2.0.29/bin/activate
module load python/3.8.1

export PATH=$PATH:/home/FCAM/vvuruputoor/i-adhore-3.0.01/bin

wgd dmd lim.cds
wgd ksd wgd_dmd/lim.cds.tsv lim.cds -o wgd_ksd_lim
wgd syn -f gene wgd_dmd/lim.cds.tsv lim.gff

wgd dmd trich_chr.cds lim.cds -o wgd_dmd_hsc
wgd ksd wgd_dmd_hsc/global_MRBH.tsv trich_chr.cds lim.cds -o wgd_hsc_ks

wgd viz -d wgd_hsc_ks/merge_focus.tsv.ks.tsv --focus2all lim.cds --extraparanomeks wgd_ksd_lim/lim.cds.tsv.ks.tsv --anchorpoints wgd_syn/iadhore-out/anchorpoints.txt -sp speciestree2.txt -o wgd_viz_all_comb --plotapgmm --plotelmm --reweight --plotkde --classic