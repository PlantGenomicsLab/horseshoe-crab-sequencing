#!/bin/bash
#SBATCH --job-name=wgd-hsc
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G
#SBATCH --mail-user=vidya.vuruputoor@uconn.edu
#SBATCH -e %x_%j.err
#SBATCH -o %x_%j.out

module load diamond
module load paml/4.9
module load blast
module load mcl
module load mafft
module load fasttree
module load gcc/8.4.0

source ~/wgd2_2.0.29/wgd2.0.29/bin/activate
module load python/3.8.1

export PATH=$PATH:/home/FCAM/vvuruputoor/i-adhore-3.0.01/bin

wgd dmd lim.cds
wgd ksd wgd_dmd/lim.cds.tsv lim.cds -o wgd_ksd_lim
wgd syn -f gene wgd_dmd/lim.cds.tsv lim.gff

wgd dmd --globalmrbh crot.cds lim.cds tgigas.cds -f lim.cds -o wgd_dmd_hsc
wgd ksd wgd_dmd_hsc/global_MRBH.tsv crot.cds lim.cds tgigas.cds -o wgd_hsc_ks

wgd viz -d wgd_hsc_ks/merge_focus.tsv.ks.tsv --focus2all lim.cds --extraparanomeks wgd_ksd_lim/lim.cds.tsv.ks.tsv -sp speciestree2.txt -o wgd_viz_mixed_Ks_try2 --anchorpoints wgd_syn/iadhore-out/anchorpoints.txt --plotapgmm --plotelmm

#wgd viz -d wgd_hsc_ks/global_MRBH.tsv.ks.tsv --focus2all lim.cds --extraparanomeks wgd_ksd_lim/lim.cds.tsv.ks.tsv --anchorpoints wgd_syn/iadhore-out/anchorpoints.txt -sp speciestree.txt -o wgd_viz_all_comb_2 --plotapgmm --plotelmm --reweight --plotkde --classic

#wgd viz -d wgd_globalmrbh_ks/global_MRBH.tsv.ks.tsv -sp speciestree.txt --focus2all pafricana.chr_longest_isoform.cds.fasta --extraparanomeks wgd_ksd/pafricana.chr_longest_isoform.cds.fasta.tsv.ks.tsv --anchorpoints wgd_syn/iadhore-out/anchorpoints.txt --plotapgmm --plotelmm