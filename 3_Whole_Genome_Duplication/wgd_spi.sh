#!/bin/bash
#SBATCH --job-name=wgd-hsc
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G
#SBATCH --mail-user=vidya.vuruputoor@uconn.edu
#SBATCH -e %x_%j.err
#SBATCH -o %x_%j.out

module load diamond
module load paml/4.9
module load blast
module load mcl
module load mafft
module load fasttree
module load gcc/8.4.0

source ~/wgd2_2.0.29/wgd2.0.29/bin/activate
module load python/3.8.1

export PATH=$PATH:/home/FCAM/vvuruputoor/i-adhore-3.0.01/bin

wgd dmd trich_chr.cds
wgd ksd wgd_dmd/trich_chr.cds.tsv trich_chr.cds -o wgd_ksd_lim
wgd syn -f mRNA wgd_dmd/trich_chr.cds.tsv trich_chr.gff