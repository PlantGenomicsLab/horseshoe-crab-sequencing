# Hox analysis

<details><summary>1. Create database with all proteins</summary>

```
module load blast/2.11.0

export TMPDIR=/home/FCAM/mneitzey/tmp

makeblastdb -in genes.fasta.faa \
-dbtype prot \
-parse_seqids \
-out lpolyphemus_2022_prots \
-title "lpolyphemus_2022_prots"
```

</details>

<details><summary>2. Blast for Hox proteins</summary>

```
database=lpolyphemus_2022_prots
queryPath=../queries/
queryFiles="Cr_AbdA-A__Cr_041036-T1.fasta Cr_AbdB-A__Cr_010330-T1.fasta Cr_Antp-A__Cr_041019-T1.fasta Cr_Dfd-A__Cr_041001-T1.fasta Cr_Ftz-A__Cr_041006-T1.fasta Cr_Lab-A__Cr_040978-T1.fasta Cr_Pb-A__Cr_040988-T1.fasta Cr_Scr-A__Cr_041004-T1.fasta Cr_Ubx-A__Cr_041030-T1.fasta Cr_Zen-A__Cr_040989-T1.fasta"

module load blast/2.11.0

for x in $queryFiles
        do
                prefix=$(echo ${x} | awk 'BEGIN { FS = ".fasta" } ; { print $1 }' )
                blastp -query ${queryPath}${x} \
                        -db $database \
                        -out ${prefix}.txt \
                        -evalue 0.05 \
                        -num_threads 1 \
                        -outfmt 6
        done
```

</details>

<details><summary>3. Align Hox sequences in geneious prime</summary>

In Geneious Prime(R) v2022.2.2, all species sequences were aligned with MAFFT v7.49084 (Algorithm: L-INS-I; Scoring matrix: BLOSUM62; Gap open penalty: 1.53; Offset value: 0.123) to generate `2023-02-03_hoxAlignment_aa.phy`.  

</details>

<details><summary>4. Generate phylogenetic tree</summary>

```
alignment=2023-02-03_hoxAlignment_aa.phy
prefix=2023-02-03_hoxAlignment_aa

module load RAxML/8.2.11

raxmlHPC-PTHREADS -s $alignment -n $prefix -m PROTGAMMAAUTO -f a -p 456 -x 456 -k -N 100 -T 20
```

</details>

<details><summary>5. Create consensus and visualize tree</summary>

The phylogenetic tree was uploaded into Geneious Prime. A consensus tree was created using "Consensus Tree Builder" --> "Create Consensus Tree" with 50% support threshold and 0% burn-in. Color and labels were added in Affinity Designer.  

</details>