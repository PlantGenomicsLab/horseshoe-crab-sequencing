# Methylation analyses

## Called & filtered methylation

<details><summary>1. Called methylation on all nanopore runs with remora</summary>

```
fast5dir=fast5_PAD59173
outsortedbam=limulus_PAD59173_methyl.sorted.bam

# Installed and ran BonitoRemora/v1.1.1 locally
source /home/FCAM/mneitzey/.bashrc
source activate BonitoRemora

module load samtools/1.9

bonito basecaller dna_r9.4.1_e8_sup@v3.3 \
    $fast5dir \
    --modified-bases 5mC \
    --reference $reference \
    --recursive \
    --alignment-threads 16 \
        | samtools view -@ 16 -bS | samtools sort -T /home/FCAM/mneitzey/tmp -m 4g -O bam -o $outsortedbam

samtools index $outsortedbam -@ 16
```

</details>

<details><summary>2. Combined methylation bams into one bed file</summary>

```
ref=26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta
bams="limulus_FAK70868_methyl.sorted.bam limulus_FAK71862_3_methyl.sorted.bam limulus_FAK71862_b_methyl.sorted.bam limulus_PAD59173_methyl.sorted.bam limulus_PAE06115_methyl.sorted.bam"
outbed=Lp_methyl_combined.bed

#######################################
### Scaffold assembly with reference
#######################################

source /home/FCAM/mneitzey/miniconda3/etc/profile.d/conda.sh
conda activate modbam2bed

modbam2bed -e -m 5mC --cpg -t 10 $ref $bams > $outbed
```
</details>

<details><summary>3. Filtered methylation & created bedgraph</summary>

```
outbed=Lp_methyl_combined.bed
prefix=Lp_methyl_combined
genomein=genome_lsort_rename.txt

#######################################
### Filter out <10X coverage
#######################################

awk -F'\t' 'BEGIN {OFS = FS} {print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$10-$14}' $outbed > ${prefix}_realcov.bed

awk '$15 >=10' ${prefix}_realcov.bed > ${prefix}_realcov_cov10.bed
echo "Filtered out coverage less than 10X"

#######################################
### Remove "nan" lines
#######################################

awk '$11 != "nan"' ${prefix}_realcov_cov10.bed > ${prefix}_realcov_cov10_nonan.bed
echo "Removed 'nan' lines"

#######################################
### Make bedgraph
#######################################

module load bedtools/2.29.0

bedtools sort -g $genomein -i ${prefix}_realcov_cov10_nonan.bed > ${prefix}_realcov_cov10_nonan.sort.bed

awk -F'\t' 'BEGIN {OFS = FS} {print $1,$2,$3,$11}' ${prefix}_realcov_cov10_nonan.sort.bed > ${prefix}_realcov_cov10_nonan.sort.bedgraph
```
</details>

## Methylation across genes (deepTools)

<details><summary>4. Converted methyl bedgraph to bigwig</summary>

```
inbg=Lp_methyl_combined_realcov_cov10_nonan.sort.bedgraph
outbed1=Lp_methyl_combined_realcov_cov10_nonan.ucsc-sort.bedgraph
chrsizes=genome_lsort_rename.txt
outbw=Lp_methyl_combined_realcov_cov10_nonan.sort.bw

module load ucsc_genome/2012.05.22

LC_COLLATE=C

sort -k1,1 -k2,2n $inbg > $outbed1

bedGraphToBigWig $outbed1 $chrsizes $outbw
```
</details>

<details><summary>5. Pulled out "gene" info from gene annotation bed file</summary>

1. Print out "genes" from .gtf file into deepTools bed format with strand
```
awk '$3 == "gene"' Lp_gene_annotations.sort.gtf | awk -F'\t' 'BEGIN {OFS = FS} {print $1,$4,$5,$9,".",$7}' | sed 's/ID=//' > Lp_gene_annotations_gene.bed
```

2. Sort bed file with bedtools
```
module load bedtools/2.29.0

bedtools sort -g genome_lsort_rename.txt -i Lp_gene_annotations_gene.bed > Lp_gene_annotations_gene.sort.bed 
```
</details>

<details><summary>6. Computed deepTools matrices</summary>

```
ins=Lp_methyl_combined_realcov_cov10_nonan.sort.bw
inr=Lp_gene_annotations_gene.sort.bed
threads=10

#################################
# Create matrix for deeptools use
#################################

module load deeptools/3.5.0

# Gene _ start codon _ reference point _ before 300bp _ after 300bp _ missing data as zero & skip zeros

b=300
a=300
out=matrix_Lp_methyl-gene_start_rp_b300_a300_na0.tab.gz

computeMatrix reference-point \
        --scoreFileName $ins \
        --regionsFileName $inr \
        --referencePoint TSS \
          --nanAfterEnd \
        --beforeRegionStartLength $b \
        --afterRegionStartLength $a \
        --numberOfProcessors $threads \
        --missingDataAsZero \
        --skipZeros \
        -out $out

# Gene _ stop codon _ reference point _ before 300bp _ after 300bp _ missing data as zero & skip zeros
b=300
a=300
out=matrix_Lp_methyl-gene_stop_rp_b300_a300_na0.tab.gz

computeMatrix reference-point \
        --scoreFileName $ins \
        --regionsFileName $inr \
        --referencePoint TES \
        --nanAfterEnd \
        --beforeRegionStartLength $b \
        --afterRegionStartLength $a \
        --numberOfProcessors $threads \
        --missingDataAsZero \
        --skipZeros \
        -out $out
```
</details>

<details><summary>7. Generated deepTools heatmaps</summary>

```
module load deeptools/3.5.0

# Gene start

plotHeatmap \
        --matrixFile matrix_Lp_methyl-gene_start_rp_b300_a300_na0.tab.gz \
        -out matrix_Lp_methyl-gene_start_rp_b300_a300_na0_z4_y1-2.pdf \
        --yAxisLabel "Mean % methylation" \
        --refPointLabel "Gene start" \
        --zMax 4 \
        --yMax 1.2

# Gene stop

plotHeatmap \
        --matrixFile matrix_Lp_methyl-gene_stop_rp_b300_a300_na0.tab.gz \
        -out matrix_Lp_methyl-gene_stop_rp_b300_a300_na0_z4_y1-2.pdf \
        --yAxisLabel "Mean % methylation" \
        --refPointLabel "Gene stop" \
        --zMax 4 \
        --yMax 1.2
```
</details>

## Methylation at genes components & repeats (violin plots)

<details><summary>8. Averaged methylation across repeats</summary>

1. Convert repeat annotations to bed file:

```
infile=Lp_repeat_annotations_classified.out
outbed=Lp_repeat_annotations_classified.bed

module load bedops/2.4.35

rmsk2bed < $infile > $outbed
```

2. Remove column 16 (only present in some columns, which throws off bedtools) with awk

3. Average methylation across repeats with bedtools map:
```
abed=Lp_repeat_annotations_classified_no16.sort.bed
bbed=Lp_methyl_combined_realcov_cov10_nonan.sort.bedgraph
covbed=Lp_methyl-to-repeats_mcov3.bed
outbed=Lp_methyl-to-repeats_mcov3_mmean.bed
genome=genome_lsort_rename.txt

module load bedtools/2.29.0

# Generate bed file with at least 3 methylation hits overlapping each feature
bedtools map -c 1 \
-o count \
-null 0 \
-g $genome \
-a $abed \
-b $bbed | \
awk '$16 >= 3' > $covbed

# Generate average methylation at features with at least 3 methylation hits
bedtools map -c 4 \
-o mean \
-null 0 \
-g $genome \
-a $covbed \
-b $bbed > $outbed
```
</details>

<details><summary>9. Averaged methylation across genes</summary>

```
abed=Lp_gene_annotations.sort.bed
bbed=Lp_methyl_combined_realcov_cov10_nonan.sort.bedgraph
covbed=Lp_methyl-to-genes_mcov3.bed
outbed=Lp_methyl-to-genes_mcov3_mmean.bed
genome=genome_lsort_rename.txt

#################################
### Get ave methyl by feature
#################################

module load bedtools/2.29.0

# Generate bed file with at least 3 methylation hits overlapping each feature
bedtools map -c 1 \
-o count \
-null 0 \
-g $genome \
-a $abed \
-b $bbed | \
awk '$11 >= 3' > $covbed

# Generate average methylation at features with at least 3 methylation hits
bedtools map -c 4 \
-o mean \
-null 0 \
-g $genome \
-a $covbed \
-b $bbed > $outbed
```
</details>

10. Made violin plots in R with `methylation_plots.R`, methylation-to-repeat bed file, and methylation-to-gene bed file.

## Methylation, CpG, repeat, and heterozygosity profiles across the genome (dotplot)

<details><summary>11. Created 100kb windowed bedfile from genome</summary>

1. Created 100kb windowed bedfile
```
module load bedtools/2.29.0

bedtools makewindows -g Lp_genome.fasta -w 100000 > Lp_genome_100kbwi.bed
```

2. Removed last truncated window from each chromosome manually 
</details>

<details><summary>12. Averaged methylation percent across windows</summary>

```
inbed=Lp_methyl_combined_realcov_cov10_nonan.sort.bedgraph
outbed=Lp_methyl_combined_realcov_cov10_nonan_100kb.bedgraph

module load bedtools/2.29.0

bedtools map -a $genome -b $inbed -c 4 -o mean -null 0 > $outbed
```

</details>

<details><summary>13. Summed CpG bases across windows</summary>

1. Created CpG bed file with fastaRegexFinder:
```
`python fastaRegexFinder.py -f Lp_genome.fasta -r CG --noreverse > CpG.bed` 
```

2. Summed CpG bp across 100kb windows with bedtools map:
```
genome=Lp_genome_100kbwi_noends.bed
inbed=Lp_CpG.bed
outbed=Lp_CpG_100kb.bedgraph

module load bedtools/2.29.0

bedtools map -a $genome -b $inbed -c 5 -o sum -null 0 > $outbed
```
</details>

<details><summary>14. Summed repeat length across windows</summary>

1. Create repeat bedgraph with repeat length in column 4

```
inbed=Lp_repeat_annotations_classified.bed
prefix=Lp_repeat

#######################################
### Print bed col 1-3
#######################################

#awk -F'\t' 'BEGIN {OFS = FS} {print $1,$2,$3}' $inbed > ${prefix}_1-3.bed

#######################################
### Merge overlapping repeats
#######################################

#module load bedtools/2.29.0

#bedtools merge -i ${prefix}_1-3.bed > ${prefix}_1-3_merge.bed

#######################################
### Print length in col 4
#######################################

awk -F'\t' 'BEGIN {OFS = FS} {print $1,$2,$3,$3-$2}' ${prefix}_1-3_merge.bed > ${prefix}_length.bed
```

2. Sum repeat length across windows
```
genome=genome_lsort_rename.txt
inbed=Lp_repeat_length.bed
inbedsort=Lp_repeat_length.sort.bed
windows=Lp_genome_100kbwi_noends.bed
outbed=Lp_repeat_length_100kb.bedgraph

module load bedtools/2.29.0

bedtools sort -g $genome -i $inbed > $inbedsort

bedtools map -c 4 -o sum -null 0 -a $windows -b $inbedsort > $outbed
```
</details>

<details><summary>15. Calculated heterozygosity across windows</summary>

Given windowed bed file to calculated heterozygosity by Noah Reid.  

1. Created per-base heterozygosity file with minimum 80% sites evaluated over 100kb windows, 10kb sliding:
```
inbed=Lp_hets_100kb_rename.bed
prefix=Lp_hets_100kb
genomein=/core/labs/Oneill/mneitzey/Limulus/2022_assembly/reorder_scaffolds/genome_lsort_rename.txt

#######################################
### Sort bed
#######################################

module load bedtools/2.29.0

bedtools sort -g $genomein -i $inbed > ${prefix}.sort.bed

#######################################
### Filter out <80% sites evaluated
#######################################

awk '$7 >=0.8' ${prefix}.sort.bed > ${prefix}_80perc.sort.bed

#######################################
### Compute per-base heterozygosity
#######################################

awk -F'\t' 'BEGIN {OFS = FS} {print $1,$2,$3,$8/$5}' ${prefix}_80perc.sort.bed > ${prefix}_80perc.sort.bedgraph
```

2. Filtered to 100kb, no slide: `/core/labs/Oneill/mneitzey/Limulus/2022_assembly/het2/bedtools_intersect_windows_het.sh`  

```
inbed=Lp_hets_100kbwi_10kbsl_80perc.sort.bedgraph
genome=Lp_genome_100kbwi_noends.bed
outbed=Lp_hets_100kbwi_80perc.bedgraph

#######################################
### Intersect files
#######################################

module load bedtools/2.29.0

bedtools intersect -F 1.0 -wb -a $genome -b $inbed | awk -F'\t' 'BEGIN {OFS = FS} {print $1,$2,$3,$7}' > $outbed
```
</details>

<details><summary>16. Intersected all 100kb windowed bedfiles</summary>

```
methylbed=Lp_methyl_combined_realcov_cov10_nonan_100kb.bedgraph
repbed=Lp_repeat_length_100kb.bedgraph
hetbed=Lp_hets_100kbwi_80perc.bedgraph
cpgbed=Lp_CpG_100kb.bedgraph
genome=Lp_genome_100kbwi_noends.bed
outbed1=Lp_met-rep_100kbwi.bedgraph
outbed2=Lp_met-rep-het_100kbwi.bedgraph
outbed3=Lp_met-rep-het-cpg_100kbwi.bedgraph

module load bedtools/2.29.0

bedtools intersect -F 1.0 -wa -wb -a $methylbed -b $repbed > $outbed1

bedtools intersect -F 1.0 -wa -wb -a $outbed1 -b $hetbed | awk -F'\t' 'BEGIN {OFS = FS} {print $1,$2,$3,$4,$8,$12}' > $outbed2

bedtools intersect -F 1.0 -wa -wb -a $outbed2 -b $cpgbed | awk -F'\t' 'BEGIN {OFS = FS} {print $1,$2,$3,$4,$5,$6,$10}' > $outbed3
```
</details>

17. Created dot plot using `cpg_methyl_rep_het_dotplots.R`  

## Assessing regions of interest (heatmap)

**Defined "regions"**
* ROI = Region of interest  
* nonROI = Upstream & downstream of ROI on the same chromosome  
* WC= Whole chromosome (for chromosomes without ROIs)  
* WG= Whole genome (for reference / comparison)  

<details><summary>18. Filtered repeat masker out file to region coordinates</summary>

```
# ROI example
awk '$5 == "Chr2"' /core/labs/Oneill/mneitzey/Limulus/2022_assembly/renamed_files/Lp_repeat_annotations_classified.out | awk '$6 >= 72900000' | awk '$7 <= 80200000' >> Lp_Chr2_72900000-80200000_repmask.out

# nonROI example
awk '$5 == "Chr2"' /core/labs/Oneill/mneitzey/Limulus/2022_assembly/renamed_files/Lp_repeat_annotations_classified.out | awk '$6 < 72900000' >> Lp_Chr2_pre-72900000_post-80200000_repmask.out

# WC example
awk '$5 == "Chr1"' /core/labs/Oneill/mneitzey/Limulus/2022_assembly/renamed_files/Lp_repeat_annotations_classified.out >> Lp_Chr1_wc_repmask.out

# Don't need to do for WG
```
</details>

<details><summary>19. Built new summary files based on regions</summary>

For `roi_lengths/Lp_${region}_genome.tsv`
```
# ROI
Chr2    7300000
# nonROI
Chr2    141681578
# WC
Chr1    185424157
# Don't need to do for WG
```

```
locals="Chr2_72900000-80200000 Chr2_pre-72900000_post-80200000 Chr1_wc"

module load RepeatMasker/4.1.2

for region in $locals
       do
               /isg/shared/apps/RepeatMasker/4.1.2/RepeatMasker/util/buildSummary.pl -species Limulus \
               -genome roi_lengths/Lp_${region}_genome.tsv \
               -useAbsoluteGenomeSize Lp_${region}_repmask.out > \
               Lp_${region}_summary.txt
       done
```
</details>

<details><summary>20. Created unique list of top 5 repeats from all regions</summary>

To help pull & sort list of repeats by bpMasked
```
for region in $locals
        do
                sed '1,/Repeat[[:space:]]Stats/d' Lp_${region}_summary.txt | \
                sed '1,/================/d' | \
                sed '/--------------------------------------------------------/Q' | \
                sed -e 's/^ *//g' | \
                sed 's/ \+ /\t/g' | \
                sort -k 3rn > \
                Lp_${region}_summary_fams.tsv
        done
```
</details>

<details><summary>21. Compiled tsv with all regions by definition, repeat, and % masked</summary>

```
# Column names (not included in final file)
FullName  RegType  Chr Coord   Repeat  Count   bpMasked    %masked
```

Example from each region (need to remove % from % masked for R input):
```
Genome  Genome  Genome  Genome  ltr-1_family-26 133658  26841105        1.44
WC_Chr1 WC      Chr1    wc      ltr-1_family-26 11332   2337126 1.26
nonROI_Chr2     nonROI  Chr2    pre-72900000_post-80200000      ltr-1_family-26 9199    1894300 1.34
ROI_Chr2        ROI     Chr2    72900000-80200000       ltr-1_family-26 274     66621   0.91
```
</details>

22. Created heatmap using `roi_heatmap.R`
