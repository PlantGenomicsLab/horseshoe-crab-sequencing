# Michelle Neitzey
# January 17, 2023
# Assess Nanopore methylation calls of Limulus polyphemus

library("ggplot2")
library(tidyverse)
version=2

###########################
### Gene methyl breakdown
###########################

genebed <- as.data.frame(read.table("Lp_methyl-to-genes_mcov3_mmean.bed",header = TRUE, sep="\t",stringsAsFactors=FALSE, quote=""))
glimpse(genebed)

genebed2 <- filter(genebed, Type != "mRNA")
glimpse(genebed2)

p <- ggplot(genebed2, aes(x=Type, y=MethylPerc, fill=Type)) + 
  geom_violin() +
  ylab("Average % methylation") +
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        axis.title.x = element_blank(),
        legend.title=element_blank(),
        legend.position = "none"
  )

p

p + stat_summary(fun.y=mean, geom="point", color="black", size = 2)

ggsave(filename = paste("Lp_methyl_exon-intron_v",version,".pdf",sep = ""), device = "pdf", path = "methylation-genes/", width = 12.5, height = 8.34, units = "in")
ggsave(filename = paste("Lp_methyl_exon-intron_v",version,".svg",sep = ""), device = "svg", path = "methylation-genes/", width = 1200, height = 800, units = "px")

#####################################
### Repeat big class methyl breakdown
#####################################

repbed <- as.data.frame(read.table("Lp_methyl-to-repeats_mcov3_mmean.bed",header = TRUE, sep="\t",stringsAsFactors=FALSE, quote=""))
glimpse(repbed)

repbed$BigClass <- gsub("\\/.*","",repbed$RepClass)
glimpse(repbed)

repbed2 <- filter(repbed, BigClass != "SINE?")

p <- ggplot(repbed2, aes(x=BigClass, y=MethylPerc, fill=BigClass)) +
  geom_violin() +
  ylab("Average % methylation") +
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        axis.title.x = element_blank(),
        legend.title=element_blank(),
        legend.position = "none"
  )

p

p + stat_summary(fun.y=mean, geom="point", color="black", size = 2)

ggsave(filename = paste("Lp_methyl_rep_bigclass_v",version,".pdf",sep = ""), device = "pdf", path = "methylation-repeats/", width = 6.77, height = 4.17, units = "in")
ggsave(filename = paste("Lp_methyl_rep_bigclass_v",version,".svg",sep = ""), device = "svg", path = "methylation-repeats/", width = 650*2, height = 400*2, units = "px")

#######################################
### Repeat small class methyl breakdown
#######################################

repbed$SmallClass <- sub(".*/", "", repbed$RepClass)
head(repbed)

dnadf <- subset(repbed, BigClass=="DNA")
glimpse(dnadf)

ltrdf <- subset(repbed, BigClass=="LTR")
glimpse(ltrdf)

linedf <- subset(repbed, BigClass=="LINE")
glimpse(linedf)

p <- ggplot(dnadf, aes(x=SmallClass, y=MethylPerc, fill=SmallClass)) +
   geom_violin() +
   ylab("Average % methylation") +
   theme(panel.grid.major = element_blank(), 
         panel.grid.minor = element_blank(),
         axis.title.x = element_blank(),
         legend.title=element_blank(),
         legend.position = "none",
         axis.text.x = element_text(size = 7, angle = 20)
   )
 
p

p + stat_summary(fun.y=mean, geom="point", color="black", size = 2)

ggsave(filename = paste("Lp_methyl_rep_smallclass_dna_v",version,".pdf",sep = ""), device = "pdf", path = "methylation-repeats/", width = 12.5, height = 4.17, units = "in")
ggsave(filename = paste("Lp_methyl_rep_smallclass_dna_v",version,".svg",sep = ""), device = "svg", path = "methylation-repeats/", width = 1200*2, height = 400*2, units = "px")


p <- ggplot(ltrdf, aes(x=SmallClass, y=MethylPerc, fill=SmallClass)) +
   geom_violin() +
   ylab("Average % methylation") +
   theme(panel.grid.major = element_blank(), 
         panel.grid.minor = element_blank(),
         axis.title.x = element_blank(),
         legend.title=element_blank(),
         legend.position = "none",
   )

p

p + stat_summary(fun.y=mean, geom="point", color="black", size = 2)

ggsave(filename = paste("Lp_methyl_rep_smallclass_ltr_v",version,".pdf",sep = ""), device = "pdf", path = "methylation-repeats/", width = 5.21, height = 4.17, units = "in")
ggsave(filename = paste("Lp_methyl_rep_smallclass_ltr_v",version,".svg",sep = ""), device = "svg", path = "methylation-repeats/", width = 500*2, height = 400*2, units = "px")

 
p <- ggplot(linedf, aes(x=SmallClass, y=MethylPerc, fill=SmallClass)) +
   geom_violin() +
   ylab("Average % methylation") +
   theme(panel.grid.major = element_blank(), 
         panel.grid.minor = element_blank(),
         axis.title.x = element_blank(),
         legend.title=element_blank(),
         legend.position = "none",
         axis.text.x = element_text(angle = 20)
   )

p

p + stat_summary(fun.y=mean, geom="point", color="black", size = 2)

ggsave(filename = paste("Lp_methyl_rep_smallclass_line_v",version,".pdf",sep = ""), device = "pdf", path = "methylation-repeats/", width = 8.33, height = 4.17, units = "in")
ggsave(filename = paste("Lp_methyl_rep_smallclass_line_v",version,".svg",sep = ""), device = "svg", path = "methylation-repeats/", width = 800*2, height = 400*2, units = "px")

# Add element count to column 
#geom_text(aes(label=..count..), y=102, stat='count', size = 3)

#######################################
### Genes vs. Repeats Methyl
#######################################
 
mrnadf <- subset(genebed, Type=="mRNA")
glimpse(mrnadf)
 
mrnadf$plot <- c("Genes")
glimpse(mrnadf)

mrnadfsub <- subset(mrnadf, select=c(Chr,Start,End,MethylPerc,plot))
glimpse(mrnadfsub)

repbed$plot <- c("Repeats")
glimpse(repbed)

repbedsub <- subset(repbed, select=c(Chr,Start,End,MethylPerc,plot))
glimpse(repbedsub)

generepbedsub <- rbind(mrnadfsub, repbedsub)
glimpse(repbedsub)

ggplot(generepbedsub, aes(x=plot, y=MethylPerc, fill=plot)) + 
  geom_violin() +
  ylab("Average % methylation") +
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        axis.title.x = element_blank(),
        legend.title=element_blank(),
        legend.position = "none"
  )

# 598 x 400 image
