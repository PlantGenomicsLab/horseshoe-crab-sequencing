# Synteny analysis

## Prep *L. polyphemus* annotations

<details><summary>1. Convert annotations from gff3 to bed</summary>

```
source ~/mcscan_env/bin/activate

python -m jcvi.formats.gff bed --type=mRNA --key=Name out.gff3 -o Lpolyphemus.bed
```

</details>

<details><summary>2. Prep CDS sequences</summary>

**A. Extract CDS sequences**
```
module load perl/5.30.1

genome=hic_26_chr_length_scaf_medaka_flye_assembly_limulus_combined_promethion_musc_blood_minion_3kb_purgedup.fasta
alignment=hic_26_chr_genome_combined_mono_interproscan_filter_multi_gene_table.txt

perl gFACs.pl \
-f gFACs_gene_table \
--entap-annotation final_annotations_no_contam_lvl1.tsv \
--no-processing \
--get-fasta \
--statistics \
--statistics-at-every-step \
--fasta "$genome" \
-O . \
"$alignment"
```

**B. Format CDS sequences for jcvi input**

```
source ~/mcscan_env/bin/activate

python -m jcvi.formats.fasta format genes.fasta Lpolyphemus.cds
```

</details>

## Prep *C. rotundicauda* annotations

<details><summary>3. Liftover annotations to chromosome-level assembly</summary>

```
conda activate LiftOff

liftoff -p 10 -infer_genes -f liftoff_features.txt -g C.rotundicauda_hic.gff3 GCA_011833715.1_IMCB_Crot_1.0_genomic_chr.fna Carcinoscorpius_rotundicauda.WCHO00000000.fa -o GCA_011833715.1_IMCB_Crot_1.0_genomic_chr_liftoff.gff3
```

</details>

<details><summary>4. Convert annotations from gff3 to bed</summary>

```
gff=GCA_011833715.1_IMCB_Crot_1.0_genomic_chr_liftoff.gff3

export PATH="/home/FCAM/mneitzey/miniconda3/bin:$PATH"
export TMPDIR=$HOME/tmp
source ~/.bashrc
source activate jcvi

#python -m jcvi.formats.gff bed --type=mRNA --primary_only $gff -o Crotundicauda_chr.bed

python -m jcvi.formats.gff bed --type=CDS --primary_only $gff -o Crotundicauda_chr_CDS.bed
```

</details>

<details><summary>5. Prep CDS sequences</summary>

**A. Extract CDS sequences with AGAT**

```
gff=GCA_011833715.1_IMCB_Crot_1.0_genomic_chr_liftoff.gff3
genome=GCA_011833715.1_IMCB_Crot_1.0_genomic_chr_fold.fna

module load singularity
img=depot.galaxyproject.org-singularity-agat-1.0.0--pl5321hdfd78af_0.img

singularity exec $img agat_sp_extract_sequences.pl -g $gff -f $genome -t cds -o Crotundicauda_chr_cds.fa
```

**B. Format CDS sequences for jcvi input**

```
export PATH="/home/FCAM/mneitzey/miniconda3/bin:$PATH"
export TMPDIR=$HOME/tmp
source ~/.bashrc
source activate jcvi

python3 -m jcvi.formats.fasta format Crotundicauda_chr_cds.fasta Crotundicauda_chr.cds
```

</details>

## Run synteny ##

<details><summary>6. Generate anchors</summary>

```
export PATH="/home/FCAM/mneitzey/miniconda3/bin:$PATH"
export TMPDIR=$HOME/tmp
source ~/.bashrc
source activate jcvi
module load last/1256
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/FCAM/mneitzey/miniconda3/lib/

python -m jcvi.compara.catalog ortholog Lpolyphemus Crotundicauda_chr --no_strip_name
```

</details>

<details><summary>7. Screen anchors</summary>

```
export PATH="/home/FCAM/mneitzey/miniconda3/bin:$PATH"
export TMPDIR=$HOME/tmp
source ~/.bashrc
source activate jcvi

python -m jcvi.compara.synteny screen --minspan=30 --simple Lpolyphemus.Crotundicauda_chr.anchors Lpolyphemus.Crotundicauda_chr.anchors.new
```

</details>

<details><summary>8. Color SDR anchors</summary>  

```
sdrblock="g18898 g18921 g18937 g18971 g19004 g19055 g19061 g19089 g19171 g19204 g19236 g19362 g19383 g19395 g19429 g19508 g19558 g19578 g19613 g19663.1 g19701 g19707 g19742 g19758 g19821 g19968 g1282.2 g35470 g35643 g35680 g35768 g35824.1 g35857 g35891 g35909 g36007 g36057.1 g36180 g36317 g36338 g36356.1 g36408 g36507 g36528 g36716 g36775.2 g36808 g37282.1 g37579 g37816 g38097 g8958.1"

cp Lpolyphemus.Crotundicauda_chr.anchors.simple Lpolyphemus.Crotundicauda_chr.colorSDR.anchors.simple

for x in $sdrblock
    do
        sed -i -e "/\<$x\>/s/^/g*&/" Lpolyphemus.Crotundicauda_chr.colorSDR.anchors.simple
    done
```

After seeing the final image, a spurious coloring occurred outside of either SDR region at this site. The `g*` coloring was manually removed.  

```
CM022393.1      20845768        20858743        Cr_018098-T1    0       +   
```

</details>

<details><summary>9. Generate Macrosynteny plot</summary>

```
export PATH="/home/FCAM/mneitzey/miniconda3/bin:$PATH"
export TMPDIR=$HOME/tmp
source ~/.bashrc
source activate jcvi

python -m jcvi.graphics.karyotype seqids layout --notex -o Lpolyphemus_Crotundicauda_SDR.pdf
```

Figure was adapted in Affinity Designer.  

</details>