# OrthoFinder

<details><summary>1. Remove proteins with >= 98% identity</summary>

Not all protein files or gffs were amenable to remove alternate isoforms. Therefore CD-HIT was used as a crude method to remove alternate isoforms.

```
list="Aethina_tumida.faa Archegozetes_longisetosus.faa Argiope_bruennichi.faa Carcinoscorpius_rotundicauda.faa Daphnia_pulex.faa Dermacentor_silvarum.faa Dermatophagoides_farinae.faa Drosophila_melanogaster.faa Galendromus_occidentalis.faa Hylyphantes_graminicola.faa Ixodes_scapularis.faa Limulus_polyphemus.faa Parasteatoda_tepidariorum.faa Phalangium_opilio.faa Rhipicephalus_microplus.faa Stegodyphus_mimosarum.faa Tachypleus_gigas.faa Tachypleus_tridentatus.faa Tetranychus_urticae.faa Tribolium_castaneum.faa"

module load cdhit/4.8.1

for x in $list
        do
                cd-hit -i ${x} -o cd-hit_${x} -c 0.98 -T 10
        done

```

</details>

<details><summary>2. Run OrthoFinder</summary>

```
/isg/shared/apps/OrthoFinder/2.5.1/orthofinder -f cd-hit/98perc_match_noTt -t 15
```

</details>

# CAFE

<details><summary>3. Make species tree ultrametric</summary>

```
module load python/2.7.14

python /isg/shared/apps/OrthoFinder/2.5.1/tools/make_ultrametric.py -r 547 SpeciesTree_rooted.txt
```

</details>

<details><summary>4. Prepare Orthogroup Gene Counts file</summary>

Based on [https://github.com/harish0201/Analyses_Pipelines/blob/main/7.CAFE.sh](https://github.com/harish0201/Analyses_Pipelines/blob/main/7.CAFE.sh)

A. Add "description" column as null; change header manually to `Desc`  
```
awk -F'\t' '{print "(null)\t"$0}' Orthogroups.GeneCount.tsv >> Orthogroups.GeneCount.Desc.tsv
```

B. Remove Total column  
```
awk -F'\t' '{$NF=""; print $0}' Orthogroups.GeneCount.Desc.tsv | rev | sed 's/^\s*//g' | rev | tr ' ' '\t' >> Orthogroups.GeneCount.Desc.NoTotal.tsv
```

C. Remove species-specific orthogroups and gene families with "> 100 gene copies in one or more species; this is necessary because big gene families cause the variance of gene copy number to be too large and lead to noninformative parameter estimates."
```
module load python/2.7.14

python /isg/shared/apps/CAFE/5.0b2/tutorial/clade_and_size_filter.py -i Orthogroups.GeneCount.Desc.NoTotal.tsv -o Nov03_cdhit98_NoTt_filtered_cafe_input.txt -s
```

</details>

<details><summary>5. Run CAFE</summary>

```
orthogroups=Nov03_cdhit98_NoTt_filtered_cafe_input.txt
ulttree=SpeciesTree_rooted_547.txt.ultrametric.tre

module load CAFE5/5.1.0

# Generate error model
cafe5 -i $orthogroups -t $ulttree -c 10 -p -e

# Run three times to ensure convergence
cafe5 -i $orthogroups -t $ulttree -c 10 -p -eresults/Base_error_model.txt -o cafe_results_1e

cafe5 -i $orthogroups -t $ulttree -c 10 -p -eresults/Base_error_model.txt -o cafe_results_2e

cafe5 -i $orthogroups -t $ulttree -c 10 -p -eresults/Base_error_model.txt -o cafe_results_3e
```

</details>

<details><summary>6. Run CAFEPlotter to summarize results</summary>

```
export PATH="/home/FCAM/mneitzey/miniconda3/bin:$PATH"
source ~/.bashrc
source activate cafeplotter-v0.2.0
export TMPDIR=$HOME/tmp

cafeplotter -i cafe_results_1e -o cafeplotter_1e --format 'pdf'
```

</details>

<details><summary>7. Summarize p<=0.05 significant results</summary>

```
summary=result_summary.tsv
list="21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 Aethina_tumida Archegozetes_longisetosus Argiope_bruennichi Carcinoscorpius_rotundicauda Daphnia_pulex Dermacentor_silvarum Dermatophagoides_farinae Drosophila_melanogaster Galendromus_occidentalis Hylyphantes_graminicola Ixodes_scapularis Limulus_polyphemus Parasteatoda_tepidariorum Phalangium_opilio Rhipicephalus_microplus Stegodyphus_mimosarum Tachypleus_gigas Tetranychus_urticae Tribolium_castaneum"

# Limulus specific
awk '{if ($2 == "Limulus_polyphemus" && $5 <= 0.05) {print}}' $summary >> result_summary_lpoly_sig05.tsv
awk '{if ($2 == "Limulus_polyphemus" && $4 > 0 && $5 <= 0.05) {print}}' $summary >> result_summary_lpoly_sig05_expand.tsv
awk '{if ($2 == "Limulus_polyphemus" && $4 < 0 && $5 <= 0.05) {print}}' $summary >> result_summary_lpoly_sig05_contract.tsv

# get counts of sig expand and contract for all nodes
for x in $list
    do
        echo $x "0.05 sig"
        awk -v x="$x" '{if ($2 == x && $5 <= 0.05) { count += 1; }} END { print count; }' $summary
        echo $x "0.05 sig expand"
        awk -v x="$x" '{if ($2 == x && $4 > 0 && $5 <= 0.05) { count += 1; }} END { print count; }' $summary
        echo $x "0.05 sig contract"
        awk -v x="$x" '{if ($2 == x && $4 < 0 && $5 <= 0.05) { count += 1; }} END { print count; }' $summary
    done
```

</details>

# Annotate significantly changing families

A .faa of the longest proteins in each significantly changing orthogroup is the input (`sig_ortho_longest.faa`)

<details><summary>8. Retrieve functional annotations from EnTAP</summary>

```
module load EnTAP/1.0.1

# Configure
EnTAP --config -t 2 --ini entap_config.ini

# Run
EnTAP --runP -i ../sig_ortho_longest.faa -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.216.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.fa.2.0.6.dmnd -d /isg/shared/databases/entap_1.0.0/eggnog_proteins.dmnd -t 8 --ini entap_config.ini
```

</details>

<details><summary>9. Retrieve COG terms from EGGNOG</summary>

```
module load eggnog-mapper/2.1.7
module unload sqlite
module load sqlite/3.31.1

emapper.py \
    -i sig_ortho_longest.faa \
    -o eggnog.blastp -m diamond --cpu 12
```

</details>