# Atlantic Horseshoe Crab (*Limulus polyphemus*) Genome

The genome assembly, annotations, and all raw data associated with this project are affiliated with NCBI BioProject PRJNA1051539.