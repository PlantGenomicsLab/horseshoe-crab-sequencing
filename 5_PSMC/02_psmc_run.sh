#!/bin/bash 
#SBATCH --job-name=psmc_run
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=10G
#SBATCH --qos=general
#SBATCH --partition=general
#SBATCH --mail-user=
#SBATCH --mail-type=END
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

# software
PSMC=~/bin/psmc/psmc

# files/directories
INDIR=../../results/smc/psmc
OUTDIR=../../results/smc/psmc
    mkdir -p ${OUTDIR}

PSMCFA=${INDIR}/maskedconsensus.psmcfa
OUTFILE=${OUTDIR}/maskedconsensus.psmc

# run psmc
${PSMC} -N25 -t15 -r5 -p "4+25*2+4+6" -o ${OUTFILE} ${PSMCFA}