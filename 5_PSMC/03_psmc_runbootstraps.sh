#!/bin/bash
#SBATCH --job-name=psmc_bootstraps
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=3G
#SBATCH --mail-user=noah.reid@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err
#SBATCH --array=[1-100]

hostname
date

# load software

# files/directories
INDIR=../../results/smc/psmc
OUTDIR=../../results/smc/psmc/bootstraps
    mkdir -p ${OUTDIR}

# run bootstraps
PSMC=~/bin/psmc/psmc
${PSMC} -N25 -t15 -r5 -b -p "2*1+20*2+4" -o ${OUTDIR}/round-${SLURM_ARRAY_TASK_ID}.psmc ${INDIR}/SPLIT_maskedconsensus.psmcfa

