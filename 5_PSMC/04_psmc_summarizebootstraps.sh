#!/bin/bash 
#SBATCH --job-name=psmc_summarizeboots
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=10G
#SBATCH --qos=general
#SBATCH --partition=general
#SBATCH --mail-user=
#SBATCH --mail-type=END
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

# software
PSMCPLOT=~/bin/psmc/utils/psmc_plot.pl

# files/directories
INDIR=../../results/smc/psmc/

cat ${INDIR}/maskedconsensus_23seg.psmc ${INDIR}/bootstraps/round-*.psmc > ${INDIR}/combined_23seg.psmc
${PSMCPLOT} -u 1e-8 -g 15 ${INDIR}/combined ${INDIR}/combined_23seg.psmc