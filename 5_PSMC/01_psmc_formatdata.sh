#!/bin/bash 
#SBATCH --job-name=psmc_formatdata
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=10G
#SBATCH --qos=general
#SBATCH --partition=general
#SBATCH --mail-user=
#SBATCH --mail-type=END
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

# load modules
module load bcftools/1.16
module load bedtools/2.29.0
module load htslib/1.16
module load samtools/1.16.1

# input/output files directories
OUTDIR=../../results/smc/psmc/
mkdir -p ${OUTDIR}


VCF=../../results/variants_clair3/merge_sorted.vcf.gz
TARGETS=../../results/coverage_stats/promethion_include_15_45_10bpmerge.bed.gz
PROBLEMREGIONS=../../metadata/problemregions.bed
GENOME=../../data/genome/genome.fa
FAI=../../data/genome/genome.fa.fai

# make a "genome" file, required by bedtools makewindows command, set variable for location
GFILE=${OUTDIR}/limulus.genome
cut -f 1-2 ${FAI} > ${GFILE}

# create new targets BED file
    # take targets inferred from coverage, remove problem regions identified from diversity plot
bedtools subtract -header -a ${TARGETS} -b ${PROBLEMREGIONS} | bgzip >${OUTDIR}/targets.bed.gz

# filter VCF file, remove non-target variants
bcftools view -m2 -M2 -v snps ${VCF} -f PASS | \
    bcftools filter -i 'GT="het"' | \
    bedtools intersect -a stdin -b ${OUTDIR}/targets.bed.gz -wa -header | \
    bgzip >${OUTDIR}/targets.vcf.gz
tabix -p vcf ${OUTDIR}/targets.vcf.gz

# create consensus sequence with IUPAC symbols to represent variants
bcftools consensus -I -f ${GENOME} --sample SAMPLE ${OUTDIR}/targets.vcf.gz >${OUTDIR}/consensus.fasta

# mask consensus sequence to exclude uncalled regions
bedtools complement -i ${OUTDIR}/targets.bed.gz -g ${GFILE} | bgzip >${OUTDIR}/exclude.bed.gz
bedtools maskfasta -fi ${OUTDIR}/consensus.fasta -fo ${OUTDIR}/maskedconsensus.fasta -bed ${OUTDIR}/exclude.bed.gz
samtools faidx ${OUTDIR}/maskedconsensus.fasta

# convert fasta to psmc input
FQ2PSMCA=~/bin/psmc/utils/fq2psmcfa
${FQ2PSMCA} ${OUTDIR}/maskedconsensus.fasta > ${OUTDIR}/maskedconsensus.psmcfa

# create a split file for bootstrapping
SPLITFA=~/bin/psmc/utils/splitfa 
${SPLITFA} ${OUTDIR}/maskedconsensus.psmcfa >${OUTDIR}/SPLIT_maskedconsensus.psmcfa