# HorseShoe Crab Sex Determination With RADSEX

This Repo contains the bioinformatic scripts and output used to process and visualize sex bias loci using the tool radsex.

### Demultiplex

Using [fgbio DemuxFastq]() we demultiplexed raw Libraries A through G from the Singapapore study.

First edit barcode file to match tool metadata input format. This script expects that you have a file newBarcode.txt in its respective pool directory. 

```bash

for fq1 in Library?/POOL?/*1.fq.gz; do

    library=$(basename $fq1 | sed 's,_1.fq.gz,,')

    dir=$(dirname $fq1)

    echo 'Sample_Barcode,Sample_Name,Library_ID,Sample_ID' > ${dir}/newBarcode.txt
    awk -F '\t' -v lib=$library -v OFS=',' 'NR>1 { print $1,$2,lib,$2}' ${dir}/Barcode.txt >> ${dir}/newBarcode.txt

    tr -d '\r' < ${dir}/newBarcode.txt > ${dir}/clnBarcode.txt

done
```

Now you can properly demultiplex the files with the tool

```bash
cd /project/pi_jpuritz_uri_edu/HSC

module load singularity/3.7.0

for fq1 in Library?/POOL?/*1.fq.gz; do

    fq2=$(echo $fq1 | sed 's,1.fq.gz,2.fq.gz,')
    prefix=$(basename $fq1 | sed 's,_1.fq.gz,,')
    barcodes=$(dirname $fq1)/clnBarcode.txt
    outdir=./demult
    mkdir -p $outdir
    echo $fq1 $fq2 $barcodes $prefix

    singularity exec fgbio:2.0.2--hdfd78af_0 fgbio --tmp-dir=. DemuxFastqs --input $fq1 $fq2 --read-structures 5B+T +T --output-type Fastq --output $outdir --metrics ${prefix}.txt --metadata $barcodes

done
```

For the *Limulus polyphemus* dataset pooled libraries were demultiplexed using process_radtags/2.62.

```bash
process_radtags -1 ~/HSC_RAD/raw_data/Pool_5_dedup.F.fq.gz -2 ~/HSC_RAD/raw_data/Pool_5_dedup.R.fq.gz -b ~/HSC_RAD/lib_1_barcodes/barcodes_pool5.txt -o ./dm_samples_5.1/ -c -q -r --inline_inline --renz_1 pstI --renz_2 ecoRI
```

Next move forward reads into seperate directory. **IMPORTANT** we Restricted analysis to individuals from Library A,E,F,G due to complications with barcode missassignments.




### Radsex

For all species we start by computing depth of markers and write into markers_table.tsv.

```bash
radsex process --input-dir ./demult --output-file markers_table.tsv --threads 16 --min-depth 5
```

And leverage this table to compute marker distribution between sexes

```bash
radsex distrib --markers-table markers_table.tsv --output-file distribution.tsv --popmap sexmap.tsv --min-depth 25 --groups Male,Female
```

In *limulus polyphemus* we also randomly subsampled to 15 samples for both sexes to check for robustness of results using `shuf -n 15 sexmap_{male,female}.tsv`.

### Visualization

Output from radsex was visualized using sgtr radsex_distrib function in R

```R
library(sgtr)
sgtr::radsex_distrib("distribution.tsv",
            groups = c("Male", "Female"),
            group_labels = c("Males", "Females"),
            output_file = "figures/radsex_distrib.png")
```

#### *limulus polyphemus* 

<img src="plots/radsex_distrib_L.png" alt="Marker distribution between Males and Females for Limulus polyphemus">

#### *Carcinoscorpius rotundicauda*

<img src="plots/radsex_distrib_C.png" alt="Marker distribution between Males and Females for Carcinoscorpius rotundicauda">

#### *Tachypleus gigas*

<img src="plots/radsex_distrib_T.png" alt="Marker distribution between Males and Females for Tachypleus gigas">