This subdirectory contains the code to reproduce our variant calls in Limulus polyphemus using the ddRAD data in this study. For full transparency, SLURM headers and software module versions are specified, but these are specific to the University of Connecticut's Xanadu cluster. 

The `scripts` subdirectory contains all scripts used, except script 01, which creates a root directory `/genome`, then copies the reference genome there and indexes it with `bwa` and script 02, which creates a root directory `/data/demux` and copies the demultiplexed ddRAD data there. 

The included scripts are as follows:

- `/scripts/03_fastq_raw.sh` - runs FastQC on all sample fastq files. 
- `/scripts/04_multiqc.sh` - aggregates FastQC reports with MultiQC. 
- `/scripts/05_align.sh` - aligns all sequences to the reference genome using `bwa mem`. 
- `/scripts/06_stacks_refmap.pl` - runs the stacks refmap.pl wrapper script to call variants against the reference genome. 
- `/scripts/07_alignQC.sh` - runs `samtools stats` on all alignments, aggregates statistics from the "SN" tables into one large table. 

The `/meta` directory contains stacks "popmap" files giving the population and sex of each individual. 