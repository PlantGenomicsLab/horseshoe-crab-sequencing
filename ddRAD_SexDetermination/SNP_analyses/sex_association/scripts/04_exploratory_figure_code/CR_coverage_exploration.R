library(tidyverse)

# read in mapping locations of RAD loci to NCBI reference genome
	# this is because Tang et al. did not use the same genomes as are found in NCBI
ncbiloc <- read.table("../../results/tangetal/alignments/carcinoscorpius.alpos.txt.gz")
	colnames(ncbiloc) <- c("radcontig","ncbiscaf","ncbipos","mapq")

# read in positions of all variants, with rad locus names 
v <- pipe("zcat ../../data/tangetal/CR/populations.snps.sorted.vcf.gz | grep -v '^#' | cut -f 1-5")
vpos <- read.table(v)
	colnames(vpos) <- c("scaf","pos","radloc","al1","al2")
	vpos$radcontig <- str_remove(vpos$radloc,":.*") %>% as.numeric()
	vpos <- vpos[,c(1,2,3,6,4,5)]

vpos <- left_join(vpos,ncbiloc)

# read in sexes
sexes <- read.csv("../../data/tangetal/sexes.csv")

# read in vcf header
f <- pipe("zcat ../../data/tangetal/CR/populations.snps.sorted.vcf.gz | head -n 30 | grep CHROM")
h <- scan(f,what="character",sep="\t")
	h <- str_remove(h,"#")

vcfsex <- left_join(data.frame(name=h),sexes,by=c(name="Sample.ID"))
	vcfsex[which(vcfsex$Sex=="Unknow"),"Sex"] <- "Unknown"
	vcfsex[is.na(vcfsex$Sex),"Sex"] <- "Unknown"

males <- which(vcfsex$Sex=="Male")
females <- which(vcfsex$Sex=="Female")

# read in coverage scaling factors
sf <- read.table("../../results/tangetal/CR_coveragescaling.txt")

# read in coverage p-values
covp <- read.table("../../results/tangetal/CR_coverageassoc.txt.gz")
	covp$V4 <- p.adjust(covp$V3,method="BH")
	colnames(covp) <- c("scaf","pos","covp","covq")
	covp <- left_join(vpos,covp)

# read in genotype p-values
genp <- read.table("../../results/tangetal/CR_genotypeassoc.txt.gz")
	genp$V7 <- p.adjust(genp$V6,method="BH")
	colnames(genp) <- c("scaf","pos","radloc","al1","al2","genp","genq")
	genp <- left_join(vpos,genp)

# join tables
pvals <- full_join(genp,covp) %>%
	arrange(scaf,pos)


chrs_by_length <- (group_by(pvals,scaf) %>% summarize(l=max(pos)) %>% arrange(-l) %>% select(scaf))[[1]]

chrscafs <- paste("Scaffold",1:16,sep="")

pvals %>% 
	mutate(scaf_fac=factor(scaf,levels=chrs_by_length)) %>%
	arrange(scaf_fac) %>%
	filter((1:(length(covq))%%5==0) & mapq==60 & scaf %in% chrscafs) %>%
	ggplot(aes(x=1:length(covq),y=-log(covq,10),col=scaf)) +
		geom_point()

# plot q values
# subset of windows 1/5
subp <- ((1:(length(pvals$covq)))%%5==0) & pvals$mapq==60

# coverage q values
plot(-log(pvals[subp,]$covq,10),pch=20,cex=.2,col=factor(pvals[subp,1]))
# genotype q values
plot(-log(pvals[subp,]$genq,10),pch=20,cex=.2,col=factor(pvals[subp,1]))


# read in depth scaling factors
depscale <- read.table("../../results/tangetal/CR_coveragescaling.txt")

# read in depth for a scaffold of interest
p <- pipe("tabix ../../data/tangetal/CR/populations.snps.depth.txt.gz Scaffold5")
dep <- read.table(p)
	# convert to numeric values
	for(i in 3:dim(dep)[2]){

		dep[,i] <- as.numeric(dep[,i])
		dep[is.na(dep[,i]),i] <- 0
		# dep[,i] <- dep[,i]/depscale[i-2,1] * median(depscale[,1])

	}
colnames(dep) <- c("scaf","pos",vcfsex[-c(1:9),1])

	# depscale <- data.frame(colSums(dep[,-c(1:2)]))

	for(i in 3:dim(dep)[2]){
		dep[,i] <- dep[,i]/depscale[i-2,1] * median(depscale[,1])
		}

# plot mds of scaled depth to cluster samples

# subsample of sites:
	subs <- dep[,2] >7e7 & dep[,2] < 7.8e7

vcfsex$newsex <- NA
vcfsex$newsex[which(colSums(dep[subs,-c(1:2)]) > 90000)+9] <- "Male"
vcfsex$newsex[which(colSums(dep[subs,-c(1:2)]) < 90000)+9] <- "Female"

newmales <- which(vcfsex$newsex=="Male")
newfemales <- which(vcfsex$newsex=="Female")

# mds plot
depdist <- dist(t(dep[subs,-c(1:2)]))
mds <- cmdscale(depdist)
	mds <- data.frame(sample=rownames(mds),mds1=mds[,1],mds2=mds[,2])
	mds <- left_join(mds,vcfsex,c(sample="name"))

p <- ggplot(mds,aes(x=mds1,y=mds2,color=Sex)) + 
	geom_point()
p


# overplot male/female depth
plot(dep[,2],rowMeans(dep[,males-7]),pch=20,cex=.2)
points(dep[,2],rowMeans(dep[,females-7]),pch=20,cex=.2,col="red")

# plot log ratio of depth

plot(
	x=dep[,2],
	y=log(rowMeans(dep[,newfemales-7])/rowMeans(dep[,newmales-7]),2),
	pch=20,cex=.2,
	ylim=c(-3,3),
	col=rgb(0,0,0,.3),
	ylab="log2 ratio female/male coverage for each SNP",
	xlab="position on Scaffold5")
abline(h=-1,col="red",lty=2,lwd=2)
abline(h=0,col="gray",lty=2,lwd=2)


# rad site positions
radpos <- mutate(pvals[,1:3],radloc=str_remove(radloc,":.*")) %>%
	filter(!(duplicated(radloc)))

filter(radpos, pos > 19e6 & pos < 20e6)


# more coverage plots
notlow <- rowMeans(dep[,c(males-7,females-7)]) > 1
nothigh <- rowMeans(dep[,c(males-7,females-7)]) < 20

lowmale <- rowMeans(dep[,males-7]) < 1
hist(rowMeans(dep[!lowmale & notlow & nothigh,females-7]),breaks=200)
hist(rowMeans(dep[!lowmale & notlow & nothigh,males-7]),breaks=200,add=TRUE,col=rgb(1,0,0,.1))

plot(
	x=rowMeans(dep[notlow & nothigh,females-7]),
	y=rowMeans(dep[notlow & nothigh,males-7]),
	pch=20,cex=.2
	)


# read in genotypes for a region of interest

f <- pipe("tabix ../../data/tangetal/CR/populations.snps.sorted.vcf.gz Scaffold5")
vcf <-read.table(f)
	for(i in 10:dim(vcf)[2]){
		vcf[,i] <- str_remove(vcf[,i],":.*")
	}
colnames(vcf) <- h


gt <- vcf[,-c(1:9)]
	for(i in 1:dim(gt)[2]){
		gt[,i] <- str_replace(gt[,i],"0/0","0")
		gt[,i] <- str_replace(gt[,i],"0/1","1")
		gt[,i] <- str_replace(gt[,i],"1/1","2")
	}
	gt <- as.matrix(gt)
	class(gt) <- "numeric"

gdist <- dist(t(gt))
gmds <- cmdscale(gdist)
	gmds <- data.frame(sample=rownames(gmds),mds1=gmds[,1],mds2=gmds[,2])
	gmds <- left_join(gmds,vcfsex,c(sample="name"))

p <- ggplot(gmds,aes(x=mds1,y=mds2,color=Sex)) + 
	geom_point()
p
