#!/bin/bash 
#SBATCH --job-name=alignments
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --mem=40G
#SBATCH --qos=general
#SBATCH --partition=general
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


hostname
date

# load modules
module load samtools/1.16.1
module load samblaster/0.1.24
module load bwa/0.7.17

# files, directories
INDIR=../../data/barrett
OUTDIR=../../results/barrett/alignments
    mkdir -p ${OUTDIR}

# limulus index, sequences
LIMIND=../../data/genome/limulus_index
LIM=${INDIR}/refmap/catalog.fa.gz

# align sequences with bwa

# limulus
bwa mem -t 7 ${LIMIND} ${LIM} | \
samblaster | \
samtools view -S -h -u - | \
samtools sort -T /scratch/limulus - >${OUTDIR}/limulus.bam

samtools index ${OUTDIR}/limulus.bam


# extract only alignment positions and mapping qualities for each rad locus
samtools view ${OUTDIR}/limulus.bam | cut -f 1,3,4,5 | gzip >${OUTDIR}/limulus.alpos.txt.gz
