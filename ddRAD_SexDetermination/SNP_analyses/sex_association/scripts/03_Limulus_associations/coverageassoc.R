library(tidyverse)

# reads from stdin
	# this file
		# ../../data/barrett/refmap/populations.snps.sorted.vcf.gz

# read in sexes
sexes <- read.table("../../data/barrett/sexmap.txt")
	colnames(sexes) <- c("Sample.ID","Sex")

# read in vcf header
f <- pipe("zcat ../../data/barrett/refmap/populations.snps.sorted.vcf.gz | head -n 30 | grep CHROM")
h <- scan(f,what="character",sep="\t")
	h <- str_remove(h,"#")

vcfsex <- left_join(data.frame(name=h),sexes,by=c(name="Sample.ID"))

males <- which(vcfsex$Sex=="M")
females <- which(vcfsex$Sex=="F")

# read in coverage scaling factors
sf <- read.table("../../results/barrett/coveragescaling.txt")

# read vcf file from stdin
f <- file("stdin")
open(f)

# for every line...
while(length(line <- readLines(f,n=1)) > 0) {
	if(grepl("^#",line)){next()}

	# split line, unlist
 	line <- str_split(line,"\\t") %>% unlist()

 	dp <- line[-c(1:2)]
 		dp <- as.numeric(dp)
 		dp[is.na(dp)] <- 0
		dp <- dp / sf[,1] * median(sf[,1])

 	pval <- wilcox.test(dp[c(males-9,females-9)] ~ vcfsex[c(males,females),]$Sex)$p.value

	out <- paste(c(line[1:2],pval),collapse="\t")
	write(out,stdout())

}
