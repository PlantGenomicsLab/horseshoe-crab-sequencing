#!/bin/bash 
#SBATCH --job-name=alignments
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --mem=40G
#SBATCH --qos=general
#SBATCH --partition=general
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


hostname
date

# load modules
module load samtools/1.16.1
module load samblaster/0.1.24
module load bwa/0.7.17

# files, directories
INDIR=../../data/tangetal
OUTDIR=../../results/tangetal/alignments
    mkdir -p ${OUTDIR}

# tachypleus index, sequences
TACHIND=../../data/genome/tachypleus
TACH=${INDIR}/TG/catalog.fa.gz

# carcinoscorpius index, sequences
CARCIND=../../data/genome/carcinoscorpius
CARC=${INDIR}/CR/catalog.fa.gz

# align sequences with bwa

# tachypleus
bwa mem -t 7 ${TACHIND} ${TACH} | \
samblaster | \
samtools view -S -h -u - | \
samtools sort -T /scratch/tachypleus - >${OUTDIR}/tachypleus.bam

samtools index ${OUTDIR}/tachypleus.bam

# carcinoscorpius
bwa mem -t 7 ${CARCIND} ${CARC} | \
samblaster | \
samtools view -S -h -u - | \
samtools sort -T /scratch/carcinoscorpius - >${OUTDIR}/carcinoscorpius.bam

samtools index ${OUTDIR}/carcinoscorpius.bam


# extract only alignment positions and mapping qualities for each rad locus
samtools view ${OUTDIR}/tachypleus.bam | cut -f 1,3,4,5 | gzip >${OUTDIR}/tachypleus.alpos.txt.gz
samtools view ${OUTDIR}/carcinoscorpius.bam | cut -f 1,3,4,5 | gzip >${OUTDIR}/carcinoscorpius.alpos.txt.gz