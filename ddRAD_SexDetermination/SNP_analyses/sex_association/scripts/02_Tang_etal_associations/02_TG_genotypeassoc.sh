#!/bin/bash
#SBATCH --job-name=TG_genotypeassoc
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 2
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=your.email@uconn.edu
#SBATCH --mem=30G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load R/4.0.3
module load htslib/1.16

VCF=../../data/tangetal/TG/populations.snps.sorted.vcf.gz
GAP=../../results/tangetal
    mkdir -p ${GAP}

zcat ${VCF} | Rscript TG_genotypeassoc.R | bgzip >${GAP}/TG_genotypeassoc.txt.gz