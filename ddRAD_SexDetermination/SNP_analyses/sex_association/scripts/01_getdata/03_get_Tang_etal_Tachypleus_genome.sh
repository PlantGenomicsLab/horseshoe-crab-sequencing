#!/bin/bash
#SBATCH --job-name=TG_genome
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 2
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=your.email@uconn.edu
#SBATCH --mem=30G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

# software
module load bwa/0.7.5a
module load samtools/1.16.1

# workdir
OUTDIR=../../data/genome
cd ${OUTDIR}

# download TG genome:
wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/014/155/125/GCA_014155125.1_IMCB_Tgig_1.0/GCA_014155125.1_IMCB_Tgig_1.0_genomic.fna.gz
gunzip GCA_014155125.1_IMCB_Tgig_1.0_genomic.fna.gz

bwa index -p tachypleus GCA_014155125.1_IMCB_Tgig_1.0_genomic.fna
samtools faidx GCA_014155125.1_IMCB_Tgig_1.0_genomic.fna