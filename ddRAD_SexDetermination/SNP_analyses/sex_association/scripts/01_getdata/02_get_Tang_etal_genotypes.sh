#!/bin/bash

# search for evidence of sex determination mechanism in 
# Tachypleus gigas and Carcinoscorpius rotundicauda

# paper:
# https://onlinelibrary.wiley.com/doi/full/10.1111/eva.13271
# data:
# https://zenodo.org/record/4988442

# sample IDs
wget https://zenodo.org/record/4988442/files/Sample_ID.xlsx

# T gigas data
wget https://zenodo.org/record/4988442/files/TG.zip

# C rotundicauda 
wget https://zenodo.org/record/4988442/files/CR.zip

# unzip
unzip TG.zip
unzip CR.zip

mkdir -p ../../data/tangetal
mv TG* ../../data/tangetal
mv CR* ../../data/tangetal
mv Sample* ../../data/tangetal

# by hand, make tables containing ID information and sex information for each species
# ALSO:
    # sort and compress and index VCF files
        # bedtools sort -header -i populations.snps.vcf | bgzip >populations.snps.sorted.vcf.gz
        # tabix -p vcf populations.snps.sorted.vcf.gz

    # extract depth tag for each genotype into a table e.g.:
        # cat /populations.snps.vcf | bcftools query -H -f '%CHROM\t%POS[\t%DP]\n' - | bgzip >populations.DEPTH.txt.gz


