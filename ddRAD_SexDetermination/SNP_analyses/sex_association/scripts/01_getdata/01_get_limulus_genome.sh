#!/bin/bash
#SBATCH --job-name=getdata
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=your.email@uconn.edu
#SBATCH --mem=10G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load samtools/1.16.1
module load bwa/0.7.5a

# genome and annotation have been produced by R. O'Neill Lab. Symlinking into this project. 

GENOMEDIR=../../data/genome
    mkdir -p ${GENOMEDIR}
ANNOTATIONDIR=../../data/annotation
    mkdir -p ${ANNOTATIONDIR}


# symlink genome
ln -s /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta ${GENOMEDIR}/genome.fa
samtools faidx ${GENOMEDIR}/genome.fa

bwa index -p ${GENOMEDIR}/limulus ${GENOMEDIR}/genome.fa

# symlink annotation(s)
ln -s /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_entap_filter_ltrs/gfacs_o/out.gff3 ${ANNOTATIONDIR}/anno.gff3
ln -s /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_entap_filter_ltrs/gfacs_o/out.gtf ${ANNOTATIONDIR}/anno.gtf

