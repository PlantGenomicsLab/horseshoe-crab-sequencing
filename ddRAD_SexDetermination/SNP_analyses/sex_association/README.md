This subdirectory contains code to produce the associations of genotype and SNP sequencing depth with sex for *Limulus polyphemus*, *Carcinoscorpius rotundicauda* and *Tachypleus gigas*, and to plot those associations across the genome. For full transparency, SLURM headers and software module versions are specified, but these are specific to the University of Connecticut's Xanadu cluster. 

The limulus scripts expect the stacks output to be in `/data/barrett`. 

Here we are using the original Tang et al. genotypes because we had trouble demultiplexing the raw sequence pools provided to us by the authors. Barcodes and pools were scrambled, and we could not confidently sort all of them out. So to analyze the full set of individuals, rather than the limited set used for RADsex, we used the published genotypes. 

A further complication is that the original Tang et al genotypes were derived from draft reference genomes that differ from the ones they ultimately published. To make results interpretable in the light of published reference genomes, we mapped the stacks consensus loci to the public reference genomes and derived new coordinates for the published SNP dataset. In order to maintain consistency across all three analyses, we also did this for Limulus polyphemus, even though we used the genome version that is being published here. 

The results are a set of p-values and new genomic coordinates that we take into R for further analysis and visualization. 

The scripts are as follows:

- `/scripts/01_getdata/01_get_limulus_genome.sh` - Copies and indexes the limulus reference genome. 
- `/scripts/01_getdata/02_get_Tang_etal_genotypes.sh` - Downloads the Tang et al. stacks output from zenodo. 
- `/scripts/01_getdata/03_get_Tang_etal_Tachypleus_genome.sh` - Downloads and indexes the Tachypleus reference from NCBI. 
- `/scripts/01_getdata/03_get_Tang_etal_Carcinoscorpius_genome.sh` - Downloads and indexes the Carcinoscorpius reference from NCBI. 
- `/scripts/02_Tang_etal_associations/01_align_RAD_loci.sh` - Aligns RAD consensus loci to NCBI reference genome. 
- `/scripts/02_Tang_etal_associations/02_TG_genotypeassoc.sh` - Does chi-square goodness of fit tests for association of genotype with sex in Tachypleus gigas. 
- `/scripts/02_Tang_etal_associations/03_TG_coverageassoc_scaling.sh` - Gets library-size scaling factors for coverage association for Tachypleus gigas. 
- `/scripts/02_Tang_etal_associations/04_TG_coverageassoc.sh` - Does Wilcoxon tests for association of SNP coverage (library-size scaled) with sex in Tachypleus gigas
- `/scripts/02_Tang_etal_associations/05_CR_genotypeassoc.sh` - Does chi-square goodness of fit tests for association of genotype with sex in Carciniscorpius rotundicauda. 
- `/scripts/02_Tang_etal_associations/06_CR_coverageassoc_scaling.sh` - Gets library-size scaling factors for coverage association for Carcinoscorpius rotundicauda. 
- `/scripts/02_Tang_etal_associations/07_CR_coverageassoc.sh` - Does Wilcoxon tests for association of SNP coverage (library-size scaled) with sex in Carcinoscorpius rotundicauda. 
- `/scripts/03_Limulus_associations/01_align_radloci.sh` - Aligns RAD consensus loci to Limulus reference genome. 
- `/scripts/03_Limulus_associations/02_genotypeassoc.sh` - Does chi-square goodness of fit tests for association of genotype with sex in Limulus polyphemus. 
- `/scripts/03_Limulus_associations/03_coverageassoc_scaling.sh` - Gets library-size scaling factors for coverage association for Limulus polyphemus. 
- `/scripts/03_Limulus_associations/04_coverageassoc.sh` - Does Wilcoxon tests for association of SNP coverage (library-size scaled) with sex in Limulus polyphemus. 

Scripts to plot sex coverage ratios and significance values for association tests can be found in the directory `/scripts/04_exploratory_figure_code`