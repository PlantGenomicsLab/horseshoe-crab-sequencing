#!/bin/bash
#SBATCH --job-name=limulus_braker_gfacs_unique_genes
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=32G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load perl

genome="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta"
alignment="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/braker/braker_rna_only_rerun/braker/augustus.hints.gff3"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

#if [ ! -d mono_o ]; then
#        mkdir mono_o multi_o
#fi
#if [ ! -d multi_o ]; then
#        mkdir multi_o
#fi

perl "$script" \
-f braker_2.05_gff3 \
--statistics \
--statistics-at-every-step \
--splice-table \
--unique-genes-only \
--get-protein-fasta \
--create-gff3 \
--create-gtf \
--fasta "$genome" \
-O gfacs_o \
"$alignment"

### python ../filtergFACsGeneTable.py --table gene_table.txt --tablePath . --idList pfam_ids.txt --idPath . --out pfam_gene_table.txt


