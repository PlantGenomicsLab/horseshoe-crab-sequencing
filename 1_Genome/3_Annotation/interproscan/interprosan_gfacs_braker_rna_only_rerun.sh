#!/bin/bash
#SBATCH --job-name=interproscan_align_gfacs_mono_output
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=32G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load interproscan/5.35-74.0

sed 's/*$//g' /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_isolate_mono_braker_rna_only_rerun/gfacs_o/genes.fasta.faa > no_introns_gfacs_mono_output.fasta

interproscan.sh -appl Pfam -i no_introns_gfacs_mono_output.fasta

sed 's/\s.*$//' no_introns_gfacs_mono_output.fasta.tsv | uniq > limulus_mono_only_pfam_ids.txt

python acer_annotations_gfacs_filtergFACsGeneTable.py --table /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_isolate_mono_braker_rna_only_rerun/gfacs_o/gene_table.txt --tablePath . --idList limulus_mono_only_pfam_ids.txt --idPath . --out limulus_mono_only_pfam_gene_table.txt

