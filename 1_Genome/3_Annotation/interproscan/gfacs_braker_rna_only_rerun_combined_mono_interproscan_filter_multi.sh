#!/bin/bash
#SBATCH --job-name=gfacs_generate_interproscan_filtered_output
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=40G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load perl

cat limulus_mono_only_pfam_gene_table.txt /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_isolate_multi_braker_rna_only_rerun/gfacs_o/gene_table.txt > combined_mono_interproscan_filter_multi_gene_table.txt

genome="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta"
alignment="combined_mono_interproscan_filter_multi_gene_table.txt"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--statistics-at-every-step \
--splice-table \
--get-protein-fasta \
--fasta "$genome" \
--create-gtf \
--create-gff3 \
-O gfacs_o \
"$alignment"

