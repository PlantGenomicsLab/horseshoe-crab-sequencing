#!/bin/bash
#SBATCH --job-name=gfacs_isolate_mono
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=15G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load perl

genome="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta"
alignment="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_braker_rna_only_rerun/gfacs_o/gene_table.txt"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--statistics \
--statistics-at-every-step \
--splice-table \
--rem-all-incompletes \
--rem-multiexonics \
--rem-genes-without-start-codon \
--rem-genes-without-stop-codon \
--rem-genes-without-start-and-stop-codon \
--get-protein-fasta \
--fasta "$genome" \
--create-gtf \
--create-gff3 \
-O gfacs_o \
"$alignment"


