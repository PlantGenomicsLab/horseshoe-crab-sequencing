#!/bin/bash
#SBATCH --job-name=gfacs_finalize_entap
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=10G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load perl

genome="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta"
alignment="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/interproscan/interproscan_gfacs_isolate_mono_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_rerun/combined_mono_interproscan_filter_multi_gene_table.txt"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--entap-annotation /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/entap/entap_gfacs_interproscan_braker_rna_only_rerun/entap_outfiles/final_results/final_annotations_no_contam_lvl1.tsv \
--no-processing \
--get-protein-fasta \
--statistics \
--statistics-at-every-step \
--splice-table \
--create-gtf \
--create-gff3 \
--fasta "$genome" \
-O gfacs_finalize_entap_o \
"$alignment"

