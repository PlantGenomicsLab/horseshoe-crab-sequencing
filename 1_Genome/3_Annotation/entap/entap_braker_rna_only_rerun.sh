#!/bin/bash
#SBATCH --job-name=entap_limulus_polyphemus
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=80G
#SBATCH --qos=general
#SBATCH -o entap_Cont%j.out
#SBATCH -e entap_Cont%j.err
#SBATCH --partition=general

module load EnTAP/0.10.8
module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/2.0.6
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0
module load eggnog-mapper/0.99.1

input_protein=/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/interproscan/interproscan_gfacs_isolate_mono_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_rerun/gfacs_o

EnTAP --runP --ini entap_config.ini -i $input_protein/genes.fasta.faa -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.208.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --threads 16
