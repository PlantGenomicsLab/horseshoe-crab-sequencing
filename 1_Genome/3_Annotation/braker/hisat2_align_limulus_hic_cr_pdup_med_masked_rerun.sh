#!/bin/bash
#SBATCH --job-name=06_align
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH --mem=50G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load hisat2/2.2.1

hisat2 -x limulus_hic_cr_pdup_med_masked_rerun \
-1 ../../reads/trimmed_SRR4215559_1.fastq -2 ../../reads/trimmed_SRR4215559_2.fastq \
-p 8 \
-S SRR4215559.sam

hisat2 -x limulus_hic_cr_pdup_med_masked_rerun \
-1 ../../reads/trimmed_SRR4215560_1.fastq -2 ../../reads/trimmed_SRR4215560_2.fastq \
-p 8 \
-S SRR4215560.sam

hisat2 -x limulus_hic_cr_pdup_med_masked_rerun \
-1 ../../reads/trimmed_SRR4215561_1.fastq -2 ../../reads/trimmed_SRR4215561_2.fastq \
-p 8 \
-S SRR4215561.sam

hisat2 -x limulus_hic_cr_pdup_med_masked_rerun \
-1 ../../reads/trimmed_SRR4215562_1.fastq -2 ../../reads/trimmed_SRR4215562_2.fastq \
-p 8 \
-S SRR4215562.sam

hisat2 -x limulus_hic_cr_pdup_med_masked_rerun \
-1 ../../reads/trimmed_SRR1145732_1.fastq -2 ../../reads/trimmed_SRR1145732_2.fastq \
-p 8 \
-S SRR1145732.sam

hisat2 -x limulus_hic_cr_pdup_med_masked_rerun \
-1 ../../reads/trimmed_SRR3987665_1.fastq -2 ../../reads/trimmed_SRR3987665_2.fastq \
-p 8 \
-S SRR3987665.sam

hisat2 -x limulus_hic_cr_pdup_med_masked_rerun \
-1 ../../reads/trimmed_SRR3984982_1.fastq -2 ../../reads/trimmed_SRR3984982_2.fastq \
-p 8 \
-S SRR3984982.sam

module unload hisat2/2.2.1

module load samtools/1.10 

samtools view -@ 8 -uhS SRR4215559.sam | samtools sort -@ 8 -T SRR4215559 -o sorted_SRR4215559.bam
samtools view -@ 8 -uhS SRR4215560.sam | samtools sort -@ 8 -T SRR4215560 -o sorted_SRR4215560.bam
samtools view -@ 8 -uhS SRR4215561.sam | samtools sort -@ 8 -T SRR4215561 -o sorted_SRR4215561.bam
samtools view -@ 8 -uhS SRR4215562.sam | samtools sort -@ 8 -T SRR4215562 -o sorted_SRR4215562.bam
samtools view -@ 8 -uhS SRR1145732.sam | samtools sort -@ 8 -T SRR1145732 -o sorted_SRR1145732.bam
samtools view -@ 8 -uhS SRR3987665.sam | samtools sort -@ 8 -T SRR3987665 -o sorted_SRR3987665.bam
samtools view -@ 8 -uhS SRR3984982.sam | samtools sort -@ 8 -T SRR3984982 -o sorted_SRR3984982.bam

samtools merge finalbamfile.bam sorted_SRR4215559.bam sorted_SRR4215560.bam sorted_SRR4215561.bam sorted_SRR4215562.bam sorted_SRR1145732.bam sorted_SRR3987665.bam sorted_SRR3984982.bam

date
