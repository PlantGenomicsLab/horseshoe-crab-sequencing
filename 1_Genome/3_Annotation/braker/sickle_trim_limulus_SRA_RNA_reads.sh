#!/bin/bash
#SBATCH --job-name=sickle_trimming
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH --mem=20G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date


module load sickle/1.33

sickle pe \
        -t sanger \
        -f /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR4215559_1.fastq -r /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR4215559_2.fastq \
        -o trimmed_SRR4215559_1.fastq -p trimmed_SRR4215559_2.fastq -s trimmed_singles_SRR4215559.fastq \
        -q 30 -l 50

sickle pe \
        -t sanger \
        -f /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR4215560_1.fastq -r /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR4215560_2.fastq \
        -o trimmed_SRR4215560_1.fastq -p trimmed_SRR4215560_2.fastq -s trimmed_singles_SRR4215560.fastq \
        -q 30 -l 50 

sickle pe \
        -t sanger \
        -f /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR4215561_1.fastq -r /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR4215561_2.fastq \
        -o trimmed_SRR4215561_1.fastq -p trimmed_SRR4215561_2.fastq -s trimmed_singles_SRR4215561.fastq \
        -q 30 -l 50

sickle pe \
        -t sanger \
        -f /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR4215562_1.fastq -r /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR4215562_2.fastq \
        -o trimmed_SRR4215562_1.fastq -p trimmed_SRR4215562_2.fastq -s trimmed_singles_SRR4215562.fastq \
        -q 30 -l 50

sickle pe \
        -t sanger \
        -f /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR1145732_1.fastq -r /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR1145732_2.fastq \
        -o trimmed_SRR1145732_1.fastq -p trimmed_SRR1145732_2.fastq -s trimmed_singles_SRR1145732.fastq \
        -q 30 -l 50

sickle pe \
        -t sanger \
        -f /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR3987665_1.fastq -r /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR3987665_2.fastq \
        -o trimmed_SRR3987665_1.fastq -p trimmed_SRR3987665_2.fastq -s trimmed_singles_SRR3987665.fastq \
        -q 30 -l 50

sickle pe \
        -t sanger \
        -f /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR3984982_1.fastq -r /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR3984982_2.fastq \
        -o trimmed_SRR3984982_1.fastq -p trimmed_SRR3984982_2.fastq -s trimmed_singles_SRR3984982.fastq \
        -q 30 -l 50



date
