#!/bin/bash
#SBATCH --job-name=07_braker
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH --mem=32G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load python/3.6.3
module load biopython/1.70
#module load N_GeneMark-ET/4.65_Perl5.32.1
module load GeneMark-ET/4.68
module load samtools/1.10
module load bamtools/2.5.1
module load blast/2.10.0
module load genomethreader/1.7.1


BRAKER_HOME=/core/cbc/Tutorials/structural_annotation_for_assembled_genomes/BRAKER/2.1.5
export PATH=${BRAKER_HOME}:${BRAKER_HOME}/scripts:${PATH}
#AUG_HOME=/core/cbc/Tutorials/software/Augustus
AUG_HOME=/home/FCAM/astarovoitov/new_augustus/Augustus
export AUGUSTUS_CONFIG_PATH=${AUG_HOME}/config
export AUGUSTUS_BIN_PATH=${AUG_HOME}/bin
#export TMPDIR=$homedir/tmp
export BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.5.1/bin
export BLAST_PATH=/isg/shared/apps/blast/ncbi-blast-2.10.0+/bin
export CDBTOOLS_PATH=/isg/shared/apps/qiime/1.9.1/cdbfasta/bin
export SAMTOOLS_PATH=/isg/shared/apps/samtools/1.9/bin
export DIAMOND_PATH=/isg/shared/apps/diamond/0.9.36/bin
#export PERL5LIB=/core/cbc/Tutorials/structural_annotation_for_assembled_genomes/perl5/lib/site_perl/5.32.1/x86_64-linux-thread-multi:/core/cbc/Tutorials/structural_annotation_for_assembled_genomes/perl5/lib/perl5:/core/cbc/Tutorials/structural_annotation_for_assembled_genomes/BRAKER/2.1.5/scripts:$PERL5LIB
export TMPDIR=/home/FCAM/astarovoitov/tmp
cp ~/gm_key_64 ~/.gm_key

BAM=/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/hisat2_alignment/hisat2_alignment_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_rerun/finalbamfile.bam
GENOME=/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/repeat_masker/rep_mask_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_rerun/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta.masked

braker.pl --genome=${GENOME} \
--bam ${BAM} \
--softmasking 1 \
--gff3 \
--cores 16




date




