#!/bin/bash
#SBATCH --job-name=interproscan_align_mono_only
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=32G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

#module load interproscan/5.35-74.0
#module unload anaconda2/4.4.0   
#module unload perl/5.28.1
#module load perl/5.24.0
#module load diamond/0.9.19
module load python/2.7.9
#sed 's/*//g' /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_finalize_entap_isolate_mono/gfacs_o/genes.fasta.faa > no_introns_final_filtered_protein_mono_only.fasta

#interproscan.sh -i no_introns_final_filtered_protein_mono_only.fasta -f gff3 -appl pfam -goterms -iprlookup
#grep ^ID no_introns_final_filtered_protein_mono_only.fasta.gff3 | cut -f1 - | uniq -  > final_annotated_pfam_mono_only.txt
#sed -i 's/^ID//g' final_annotated_pfam_mono_only.txt 
#sed -i 's/COMP/;COMP/g' final_annotated_pfam_mono_only.txt

#grep -i -e "gag-polypeptide" -e "retrotransposon" -e "reverse transcriptase" no_introns_final_filtered_protein_mono_only.fasta.gff3 > retrodomains_mono_only.txt
#cut -f1 retrodomains_mono_only.txt | uniq - > genes_with_reterodomains_mono_only.txt
#sed -i 's/^ID//g' genes_with_reterodomains_mono_only.txt 
#sed -i 's/COMP/;COMP/g' genes_with_reterodomains_mono_only.txt

python removeRetero.py --gff out.gtf --path /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_finalize_entap_isolate_mono/gfacs_o/ --nameList final_annotated_pfam_mono_only.txt --reteroList genes_with_reterodomains_mono_only.txt --out entap_gfacs_interpro_removeRetero_mono_only.gtf

