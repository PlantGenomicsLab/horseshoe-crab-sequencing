# Limulus polyphemus genome

## Introduction
The Atlantic Horseshoe Crab Limulus polyphemus is a valuable organism in the study of arthropod evolution due to its stable morphotype over ~445 million years, and the unique immune response exhibited by its blood amebocyte cells is exploited by the biomedical industry to detect bacterial endotoxins. In addition, the growing depletion of Limulus polyphemus populations has raised interest in conducting population studies to aid conservation efforts. By generating a high quality genome assembly and annotation for this organism we can shed valuable insight into these areas of interest.

## Sampling and Sequencing:
Limulus polyphemus tissue and blood samples were collected from one male specimen at Eastern Point Beach in Groton, Connecticut. Three MinION long-read sequencing runs were generated using muscle tissue. Two PromethION sequencing runs were generated, one from muscle tissue and the other from a blood sample. 

| Sequencing Run | # of Reads | Avg Read Len | Coverage |
|-|-|-|-|
| MinION 24APR19_HsC-circul | 3,058,763 | 5,161.03 | 8.52 |
| MinION 24APR19_HsC-SPRIselect | 2,933,829 | 3,356.78 | 5.32 |
| MinION 30APR19_HsC-circul-full | 1,300,181 | 6,823.75 | 4.78 |
| PromethION Muscle | 11,981,507 | 3,499.71 | 22.63 |
| PromethION Blood | 9,157,555 | 3,513.02 | 17.37 |

## Sequencing Quality Control:
All long-read sets were screened for contamination using Centrifuge v.1.0.4-beta against a database of archaea, bacterial, fungal, protozoan, and viral sequences obtained from NCBI’s RefSeq, and contaminated sequences were subsequently removed using BBTools v.38.94.

<details><summary>1. Create centrifuge db</summary>

Full script: [centrifuge_build_index.sh](centrifuge/centrifuge_build_index.sh)

```
module load centrifuge/1.0.4-beta
module load blast/2.7.1

centrifuge-download -P 32 -o library -m -d "viral" refseq > seqid2taxid.map
centrifuge-download -P 32 -o library -m -d "archaea" refseq >> seqid2taxid.map
centrifuge-download -P 32 -o library -m -d "protozoa" refseq >> seqid2taxid.map
centrifuge-download -P 32 -o library -m -d "fungi" refseq >> seqid2taxid.map
centrifuge-download -P 32 -o library -m -d "bacteria" refseq >> seqid2taxid.map

cat library/archaea/*.fna > input-sequences.fna
cat library/bacteria/*.fna >> input-sequences.fna
cat library/viral/*.fna >> input-sequences.fna
cat library/fungi/*.fna >> input-sequences.fna
cat library/protozoa/*.fna >> input-sequences.fna

/home/FCAM/astarovoitov/centrifuge_loc/bin/centrifuge-build -p 48 --conversion-table seqid2taxid.map \
--taxonomy-tree taxonomy/nodes.dmp --name-table taxonomy/names.dmp \
input-sequences.fna abfpv
```

</details>

<details><summary>2 Run Centrifuge</summary>

All scripts here: [run_centrifuge_[].sh](centrifuge/)

```
module load centrifuge/1.0.4-beta

centrifuge -x abfpv -p 16 --report-file centrifuge_limulus_minion_24APR19_HsC-SPRIselect_reads_report.tsv --quiet --min-hitlen 30 -q /archive/labs/wegrzyn/genomes/horseshoe_crab/reads/minion/24APR19_HsC-SPRIselect_FAK71862_LSK109_Min106/24APR19_HsC-SPRIselect_FAK71862_LSK109_Min106/20190424_1733_MN17911_FAK71862_63a34bb5/fastq/pass/24APR19_HsC-SPRIselect_combined_reads.fq
```

</details>

<details><summary>3. Filter long-reads</summary>

Full script: [filter_limulus_contam_reads.sh](centrifuge/filter_limulus_contam_reads.sh)

```
for i in minion_30APR19_HsC-circul-full_3627649 minion_24APR19_HsC-SPRIselect_3627609 minion_24APR19_HsC-circul_3627613 promethion_muscle_3627544 promethion_blood_3627513; do
	grep -vw "unclassified" centrifuge_limulus_"$i".out > centrifuge_limulus_"$i"_contaminated_reads.txt
	awk NF=1 centrifuge_limulus_"$i"_contaminated_reads.txt > centrifuge_limulus_"$i"_contaminated_read_ids.txt
	sort -u centrifuge_limulus_"$i"_contaminated_read_ids.txt > no_dup_centrifuge_limulus_"$i"_contaminated_read_ids.txt
done

grep -vw "unclassified" centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_3700319.out > centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_contaminated_reads.txt
awk NF=1 centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_contaminated_reads.txt > centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_contaminated_read_ids.txt
sort -u centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_contaminated_read_ids.txt > no_dup_centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_contaminated_read_ids.txt

/home/FCAM/astarovoitov/bbmap/filterbyname.sh in=/archive/labs/wegrzyn/genomes/horseshoe_crab/reads/minion/24APR19_HsC-circul_FAK70868_LSK109_Min106/24APR19_HsC-circul_FAK70868_LSK109_Min106/20190424_1909_MN17898_FAK70868_5c9a2451/fastq/pass/24APR19_HsC-circul_combined_reads.fq out=bb_test_rmv_contam_24APR19_HsC-circul_combined_reads.fq names=no_dup_centrifuge_limulus_minion_24APR19_HsC-circul_3627613_contaminated_read_ids.txt include=f
```

</details>

| Sequencing Run | # of Reads | Avg Read Len | Coverage |
|-|-|-|-|
| MinION 24APR19_HsC-circul | 3,012,029 | 5,109.93 | 8.31 |
| MinION 24APR19_HsC-SPRIselect | 2,908,337 | 3,339.84 | 5.24 |
| MinION 30APR19_HsC-circul-full | 1,282,929 | 6,740.14 | 4.67 |
| PromethION Muscle | 11,879,426 | 3,472.89 | 22.27 |
| PromethION Blood | 9,121,060 | 3,495.11 | 17.21 |

## Genome Assembly:
A draft genome assembly was generated with Flye v.2.9 using the filtered long-reads. The draft assembly was error-corrected with Medaka v.1.5.0, and its redundancy was reduced using purge-dups v.1.0.0.

<details><summary>1. Flye Assembly</summary>

Full script: [flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.sh](1_Genome Assembly/flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.sh)

```
module load flye/2.9

flye --nano-raw /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rmv_contam_24APR19_HsC-circul_combined_reads.fq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rmv_contam_24APR19_HsC-SPRIselect_combined_reads.fq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rerun_rmv_contam_29MAY2019_HsC-male-blood_PAD59173_PRO002_LSK109-reads-pass.fastq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rmv_contam_30APR19_HsC-circul-full_combined_reads.fq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rmv_contam_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21.fastq --genome-size 1.17g --out-dir flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam --threads 40
```

</details>

<details><summary>2. Medaka Error Correction</summary>

Full script: [run_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_min_rmv_contam.sh](2_Refine_Assembly/medaka/run_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_min_rmv_contam.sh)

```
module load medaka/1.5.0

medaka_consensus -i limulus_pro_superbasecall_musc_blood_min_reads_rmv_contam.fastq -d assembly.fasta -o medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_min_rmv_contam -t 64 -m r941_prom_high_g303
```

</details>


<details><summary>3. purge-dups</summary>

Full script: [run_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.sh](2_Refine_Assembly/purge_dups/run_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.sh)  

```
module load minimap2/2.22
module load python/3.6.3
module load purge_dups/1.0.0

run_purge_dups.py -p bash config_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.json /isg/shared/apps/purge_dups/1.0.0/src/ purge_dup
```

Config file: [config_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.json](2_Refine_Assembly/purge_dups/config_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.json)  
<details><summary>Command to generate config file:</summary>

Fofn file for combined input reads path: [pb_updated.fofn](2_Refine_Assembly/purge_dups/pb_updated.fofn)  

```
/scripts/pd_config.py -l /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/purge_dups/purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam -n config_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.json /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/medaka/medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_min_rmv_contam/consensus.fasta pb_updated.fofn
```

</details>

</details>

| Assembly | Genome Size | # of Contigs | N50 | BUSCO |
|-|-|-|-|-|
| Draft Flye Assembly | 1,971,092,249 | 16,167 | 2,113,424 | C:95.3%[S:79.3%,D:16.0%] |
| Medaka Error Corrected Flye Assembly | 1,978,228,054 | 16,165 | 2,119,501 | C:95.5%[S:79.4%,D:16.1%] |
| Post purge-dups Medaka Error Corrected Flye Assembly | 1,952,210,722 | 15,487 | 2,173,427 | C:95.5%[S:80.0%,D:15.5%] |

## Hi-C Scaffolding:
The Hi-C sequencing read set was generated using muscle tissue. The polished genome assembly was organized into chromosome-length scaffolds using the 3ddna pipeline. The Hi-C map was visualized and corrected using Juicebox. Hi-C scaffolding resulted in a final genome assembly comprised of 26 chromosome-length scaffolds. 

<details><summary>1. Index and align</summary>

Full script: [limulus_polyphemus_S3HiC_clean_reads_align_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.sh](2_Refine_Assembly/hic/limulus_polyphemus_S3HiC_clean_reads_align_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.sh)

```
module load bwa/0.7.17
module load samtools/1.7
module load samblaster/0.1.24

bwa index /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/purge_dups/purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/ispb_0/consensus/seqs/consensus.purged.fa
bwa mem -5SP -t 36 /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/purge_dups/purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/ispb_0/consensus/seqs/consensus.purged.fa /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/fastq/limulus_polyphemus_S3HiC_bpv_unclassified_R1.fastq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/fastq/limulus_polyphemus_S3HiC_bpv_unclassified_R2.fastq | samblaster | samtools view -S -h -b -F 2316 > limulus_polyphemus_S3HiC_clean_reads_align_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_samblaster.bam
```

</details>

<details><summary>2. Juicer</summary>

Full script: [juicer_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.sh](2_Refine_Assembly/hic/juicer_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.sh)

```
module load java-sdk/1.8.0_92
module load bwa/0.7.17

python /home/FCAM/astarovoitov/juicedir/juicer/misc/generate_site_positions.py Sau3AI hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/purge_dups/purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/ispb_0/consensus/seqs/consensus.purged.fa

awk 'BEGIN{OFS="\t"}{print $1, $NF}' hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_Sau3AI.txt > hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.chrom.sizes

bash /home/FCAM/astarovoitov/juicedir/juicer/SLURM/scripts/juicer.sh -g hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam -y hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_Sau3AI.txt -z /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/purge_dups/purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/ispb_0/consensus/seqs/consensus.purged.fa -p hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.chrom.sizes
```

</details>

<details><summary>3. 3ddna</summary>

Full script: [3ddna_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.sh](2_Refine_Assembly/hic/3ddna_hic_clean_reads_coarse_resolution_250000/3ddna_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.sh)

```
export TMPDIR="hic_tmp"

bash /home/FCAM/astarovoitov/3d-dna/run-asm-pipeline.sh --editor-repeat-coverage 4 --splitter-coarse-resolution 250000 /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/purge_dups/purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/ispb_0/consensus/seqs/consensus.purged.fa /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/aligned/merged_nodups.txt
```

</details>

<details><summary>4. 3ddna-post</summary>

Full script: [limulus_3ddna_post_hic_clean_read_splitter_res_250000.sh](2_Refine_Assembly/hic/limulus_3ddna_post_hic_clean_read_splitter_res_250000.sh)

```
export TMPDIR="hic_tmp"

bash /home/FCAM/astarovoitov/3d-dna/run-asm-pipeline-post-review.sh -r 26_chr_consensus.purged.final.review.assembly /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/purge_dups/purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/ispb_0/consensus/seqs/consensus.purged.fa /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/aligned/merged_nodups.txt
```

</details>

| Genome Size | # of Contigs | N50 | BUSCO |
|-|-|-|-|
| 1,866,915,985 | 26 | 83,029,931 | C:95.6%[S:80.1%,D:15.5%] |

![HiC Map](2_Refine_Assembly/hic/3ddna_hic_clean_reads_coarse_resolution_250000/3ddna_26_chr.PNG)

## Annotation:

### Identification and Masking of Repetitive Elements:
A de novo repeat library was generated from the genome assembly using RepeatModeler v.2.01. The genome assembly was soft-masked with RepeatMasker v.4.1.2. 

<details><summary>1. RepeatModeler Create DB</summary>

Full script: [create_rm_db_26_chr_length_assembly.sh](3_Annotation/repeat_modeler/create_rm_db_26_chr_length_assembly.sh)

```
module load RepeatModeler/2.01
ln -s /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta 26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta

BuildDatabase -name "limulus_hic_cr_pdup_med_db" /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta
```

</details>

<details><summary>2. RepeatModeler</summary>

Full script: [repeat_modeler_26_chr_length_assembly.sh](3_Annotation/repeat_modeler/repeat_modeler_26_chr_length_assembly.sh)

```
module load RepeatModeler/2.01
RepeatModeler -pa 30 -database limulus_hic_cr_pdup_med_db -LTRStruct
```

</details>

<details><summary>3. RepeatMasker</summary>

Full script: [repeat_masker_26_chr_length_assembly.sh](3_Annotation/repeat_masker/repeat_masker_26_chr_length_assembly.sh)

```
module load RepeatMasker/4.1.2

RepeatMasker -pa 32 -lib consensi.fa -gff -a -noisy -xsmall 26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta
```

</details>

Repeat Stats:
| Type of Element | Number of Elements |
|-|-|
| SINEs | 88,988 |
| Penelope | 94,207 |
| LINEs | 349,118 |
| LTR elements | 709,629 |
| DNA transposons | 316,094 |
| Rolling-circles | 8,781 |
| Unclassified | 1,558,868 |
| Small RNA | 36,212 |
| Satellites | 2,588 |

Bases masked: 704,561,311 (37.74 %)

### Gene Prediction and Refinement:
Raw SRA RNA reads were trimmed with Sickle v.1.33. Trimmed SRA RNA reads were aligned to the soft-masked genome assembly using Hisat2 v.2.2.1. Initial gene prediction was performed using BRAKER v.2.1.5. Initial gene predictions were filtered for unique genes only using gFACs v.1.1.3. Mono-exonic genes were aligned to the Pfam database using Interproscan v.5.35-74.0 and genes with alignments were retained, and genes with exclusively GO domains characteristic of LTRs (gag-polypeptide, retrotransposon, reverse transcriptaseX) were filtered. The filtered mono-exonic genes were then combined back with the isolated multi-exonic genes and a new set of annotation files was generated with gFACs v.1.1.3. Finally, the combined genes were functionally annotated with EnTAP v.0.10.8.

<details><summary>1. Sickle Trimming</summary>

Full script: [sickle_trim_limulus_SRA_RNA_reads.sh](3_Annotation/braker/sickle_trim_limulus_SRA_RNA_reads.sh)

```
module load sickle/1.33

sickle pe \
        -t sanger \
        -f /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR4215559_1.fastq -r /core/labs/Oneill/pisaac/Paulisaac/Limulus_Transcriptome/2Raw_Reads/SRR4215559_2.fastq \
        -o trimmed_SRR4215559_1.fastq -p trimmed_SRR4215559_2.fastq -s trimmed_singles_SRR4215559.fastq \
        -q 30 -l 50

```

</details>

<details><summary>2. Hisat2 Index</summary>

Full script: [hisat2_index_limulus_hic_cr_pdup_med_masked_rerun.sh](3_Annotation/braker/hisat2_index_limulus_hic_cr_pdup_med_masked_rerun.sh)

```
module load hisat2/2.2.1

hisat2-build -p 8 /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/repeat_masker/rep_mask_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_rerun/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta.masked limulus_hic_cr_pdup_med_masked_rerun
```

</details>

<details><summary>3. Hisat2 Align</summary>

Full script: [hisat2_align_limulus_hic_cr_pdup_med_masked_rerun.sh](3_Annotation/braker/hisat2_align_limulus_hic_cr_pdup_med_masked_rerun.sh)

```
module load hisat2/2.2.1

hisat2 -x limulus_hic_cr_pdup_med_masked_rerun \
-1 ../../reads/trimmed_SRR4215559_1.fastq -2 ../../reads/trimmed_SRR4215559_2.fastq \
-p 8 \
-S SRR4215559.sam
samtools view -@ 8 -uhS SRR4215559.sam | samtools sort -@ 8 -T SRR4215559 -o sorted_SRR4215559.bam
```

</details>

Alignment Stats for each read set
| Read Set | Alignment Rate |
|-|-|
| SRR4215559 | 91.64 |
| SRR4215560 | 90.56 |
| SRR4215561 | 91.11 |
| SRR4215562 | 90.30 |
| SRR1145732 | 81.23 |
| SRR3987665 | 70.40 |
| SRR3984982 | 75.96 |

<details><summary>4. BRAKER</summary>

Full script: [braker_rna_only_rerun.sh](3_Annotation/braker/braker_rna_only_rerun.sh)

```
module load python/3.6.3
module load biopython/1.70
module load GeneMark-ET/4.68
module load samtools/1.10
module load bamtools/2.5.1
module load blast/2.10.0
module load genomethreader/1.7.1


BRAKER_HOME=/core/cbc/Tutorials/structural_annotation_for_assembled_genomes/BRAKER/2.1.5
export PATH=${BRAKER_HOME}:${BRAKER_HOME}/scripts:${PATH}
AUG_HOME=/home/FCAM/astarovoitov/new_augustus/Augustus
export AUGUSTUS_CONFIG_PATH=${AUG_HOME}/config
export AUGUSTUS_BIN_PATH=${AUG_HOME}/bin
export BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.5.1/bin
export BLAST_PATH=/isg/shared/apps/blast/ncbi-blast-2.10.0+/bin
export CDBTOOLS_PATH=/isg/shared/apps/qiime/1.9.1/cdbfasta/bin
export SAMTOOLS_PATH=/isg/shared/apps/samtools/1.9/bin
export DIAMOND_PATH=/isg/shared/apps/diamond/0.9.36/bin
export TMPDIR=/home/FCAM/astarovoitov/tmp
cp ~/gm_key_64 ~/.gm_key

BAM=/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/hisat2_alignment/hisat2_alignment_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_rerun/finalbamfile.bam
GENOME=/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/repeat_masker/rep_mask_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_rerun/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta.masked

braker.pl --genome=${GENOME} \
--bam ${BAM} \
--softmasking 1 \
--gff3 \
--cores 16

```

</details>

### Braker initial stats
Total Protein Sequences: 54,605  
BUSCO: C:92.3%[S:70.9%,D:21.4%]  

<details><summary>5. Inital gFACs</summary>

Full script: [gfacs_braker_rna_only_rerun.sh](3_Annotation/gfacs/gfacs_braker_rna_only_rerun.sh)

```
module load perl

genome="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta"
alignment="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/braker/braker_rna_only_rerun/braker/augustus.hints.gff3"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f braker_2.05_gff3 \
--statistics \
--statistics-at-every-step \
--splice-table \
--unique-genes-only \
--get-protein-fasta \
--create-gff3 \
--create-gtf \
--fasta "$genome" \
-O gfacs_o \
"$alignment"

```

</details>

<details><summary>6. Interproscan and LTR Filtering</summary>

**Part 1: Interproscan Filtering**  

<details><summary>a. Isolate monoexonic genes</summary>

Full script: [gfacs_braker_rna_only_rerun_isolate_mono.sh](3_Annotation/interproscan/gfacs_braker_rna_only_rerun_isolate_mono.sh)

```
module load perl

genome="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta"
alignment="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_braker_rna_only_rerun/gfacs_o/gene_table.txt"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--statistics \
--statistics-at-every-step \
--splice-table \
--rem-all-incompletes \
--rem-multiexonics \
--rem-genes-without-start-codon \
--rem-genes-without-stop-codon \
--rem-genes-without-start-and-stop-codon \
--get-protein-fasta \
--fasta "$genome" \
--create-gtf \
--create-gff3 \
-O gfacs_o \
"$alignment"

```

</details>

<details><summary>b. Isolate multiexonic genes</summary>

Full script: [gfacs_braker_rna_only_rerun_isolate_multi.sh](3_Annotation/interproscan/gfacs_braker_rna_only_rerun_isolate_multi.sh)

```
module load perl

genome="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta"
alignment="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_braker_rna_only_rerun/gfacs_o/gene_table.txt"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--statistics \
--statistics-at-every-step \
--splice-table \
--rem-monoexonics \
--get-protein-fasta \
--fasta "$genome" \
--create-gtf \
--create-gff3 \
-O gfacs_o \
"$alignment"

```

</details>

<details><summary>c. Run Interproscan and keep genes with alignment</summary>

Full script: [interprosan_gfacs_braker_rna_only_rerun.sh](3_Annotation/interproscan/interprosan_gfacs_braker_rna_only_rerun.sh)

```
module load interproscan/5.35-74.0

sed 's/*$//g' /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_isolate_mono_braker_rna_only_rerun/gfacs_o/genes.fasta.faa > no_introns_gfacs_mono_output.fasta

interproscan.sh -appl Pfam -i no_introns_gfacs_mono_output.fasta

sed 's/\s.*$//' no_introns_gfacs_mono_output.fasta.tsv | uniq > limulus_mono_only_pfam_ids.txt

python acer_annotations_gfacs_filtergFACsGeneTable.py --table /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_isolate_mono_braker_rna_only_rerun/gfacs_o/gene_table.txt --tablePath . --idList limulus_mono_only_pfam_ids.txt --idPath . --out limulus_mono_only_pfam_gene_table.txt


```

</details>

<details><summary>d. Generate gFACs run for combined Interproscan filtered monoexonics + multiexonics</summary>

Full script: [gfacs_braker_rna_only_rerun_combined_mono_interproscan_filter_multi.sh](3_Annotation/interproscan/gfacs_braker_rna_only_rerun_combined_mono_interproscan_filter_multi.sh)

```
module load perl

cat limulus_mono_only_pfam_gene_table.txt /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_isolate_multi_braker_rna_only_rerun/gfacs_o/gene_table.txt > combined_mono_interproscan_filter_multi_gene_table.txt

genome="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta"
alignment="combined_mono_interproscan_filter_multi_gene_table.txt"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--statistics-at-every-step \
--splice-table \
--get-protein-fasta \
--fasta "$genome" \
--create-gtf \
--create-gff3 \
-O gfacs_o \
"$alignment"

```

</details>

<details><summary>e. Functional annotation with EnTAP for combined Interproscan filtered monoexonics + multiexonics</summary>

Full script: [entap_braker_rna_only_rerun.sh](3_Annotation/entap/entap_braker_rna_only_rerun.sh)

```
module load EnTAP/0.10.8
module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/2.0.6
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0
module load eggnog-mapper/0.99.1

input_protein=/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/interproscan/interproscan_gfacs_isolate_mono_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_rerun/gfacs_o

EnTAP --runP --ini entap_config.ini -i $input_protein/genes.fasta.faa -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.208.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --threads 16

```

</details>

<details><summary>f. Generate gFACs with EnTAP for combined Interproscan filtered monoexonics + multiexonics</summary>

Full script: [gfacs_finalize_entap_braker_rna_only_rerun.sh](3_Annotation/entap/gfacs_finalize_entap_braker_rna_only_rerun.sh)

```
module load perl

genome="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta"
alignment="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/interproscan/interproscan_gfacs_isolate_mono_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_rerun/combined_mono_interproscan_filter_multi_gene_table.txt"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--entap-annotation /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/entap/entap_gfacs_interproscan_braker_rna_only_rerun/entap_outfiles/final_results/final_annotations_no_contam_lvl1.tsv \
--no-processing \
--get-protein-fasta \
--statistics \
--statistics-at-every-step \
--splice-table \
--create-gtf \
--create-gff3 \
--fasta "$genome" \
-O gfacs_finalize_entap_o \
"$alignment"

```

</details>

**Part 2: Filter LTRs**  

<details><summary>a. Isolate monoexonic genes</summary>

Full script: [gfacs_finalize_entap_isolate_mono.sh](3_Annotation/filter_ltr/gfacs_finalize_entap_isolate_mono.sh)

```
module load perl

genome="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta"
alignment="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/entap/entap_gfacs_interproscan_braker_rna_only_rerun/gfacs_finalize_entap_o/gene_table.txt"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--statistics \
--statistics-at-every-step \
--splice-table \
--rem-all-incompletes \
--rem-multiexonics \
--rem-genes-without-start-codon \
--rem-genes-without-stop-codon \
--rem-genes-without-start-and-stop-codon \
--get-protein-fasta \
--fasta "$genome" \
--create-gtf \
--create-gff3 \
-O gfacs_o \
"$alignment"

```

</details>

<details><summary>b. Isolate multiexonic genes</summary>

Full script: [gfacs_finalize_entap_isolate_multi.sh](3_Annotation/filter_ltr/gfacs_finalize_entap_isolate_multi.sh)

```
module load perl

genome="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta"
alignment="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/entap/entap_gfacs_interproscan_braker_rna_only_rerun/gfacs_finalize_entap_o/gene_table.txt"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--statistics \
--statistics-at-every-step \
--splice-table \
--rem-monoexonics \
--get-protein-fasta \
--fasta "$genome" \
--create-gtf \
--create-gff3 \
-O gfacs_o \
"$alignment"

```

</details>

<details><summary>c. Run Interproscan on monoexonic genes and remove genes with LTR domains</summary>

Full script: [interproscan_finalize_entap.sh](3_Annotation/filter_ltr/interproscan_finalize_entap.sh)

```
#module load interproscan/5.35-74.0
#module unload anaconda2/4.4.0   
#module unload perl/5.28.1
#module load perl/5.24.0
#module load diamond/0.9.19
module load python/2.7.9
#sed 's/*//g' /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_finalize_entap_isolate_mono/gfacs_o/genes.fasta.faa > no_introns_final_filtered_protein_mono_only.fasta

#interproscan.sh -i no_introns_final_filtered_protein_mono_only.fasta -f gff3 -appl pfam -goterms -iprlookup
#grep ^ID no_introns_final_filtered_protein_mono_only.fasta.gff3 | cut -f1 - | uniq -  > final_annotated_pfam_mono_only.txt
#sed -i 's/^ID//g' final_annotated_pfam_mono_only.txt 
#sed -i 's/COMP/;COMP/g' final_annotated_pfam_mono_only.txt

#grep -i -e "gag-polypeptide" -e "retrotransposon" -e "reverse transcriptase" no_introns_final_filtered_protein_mono_only.fasta.gff3 > retrodomains_mono_only.txt
#cut -f1 retrodomains_mono_only.txt | uniq - > genes_with_reterodomains_mono_only.txt
#sed -i 's/^ID//g' genes_with_reterodomains_mono_only.txt 
#sed -i 's/COMP/;COMP/g' genes_with_reterodomains_mono_only.txt

python removeRetero.py --gff out.gtf --path /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_finalize_entap_isolate_mono/gfacs_o/ --nameList final_annotated_pfam_mono_only.txt --reteroList genes_with_reterodomains_mono_only.txt --out entap_gfacs_interpro_removeRetero_mono_only.gtf

```

</details>

<details><summary>d. Generate gFACs for LTR filtered monoexonics</summary>

Full script: [gfacs_generate_final_entap_ltr_filtered_monoexonic.sh](3_Annotation/filter_ltr/gfacs_generate_final_entap_ltr_filtered_monoexonic.sh)

```
genome="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta"
alignment="entap_gfacs_interpro_removeRetero_mono_only.gtf"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gtf \
--statistics \
--statistics-at-every-step \
--no-processing \
--splice-table \
--get-protein-fasta \
--fasta "$genome" \
--create-gtf \
--create-gff3 \
-O gfacs_o \
"$alignment"

```

</details>

<details><summary>e. Generate gFACs for combined LTR filtered monoexonics + multiexonics</summary>

Full script: [gfacs_final_entap_proteins_filter_ltr.sh](3_Annotation/filter_ltr/gfacs_final_entap_proteins_filter_ltr.sh)

```
module load perl/5.28.1

cat /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/filter_ltr/gfacs_o/gene_table.txt /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/annotation/gfacs/gfacs_finalize_entap_isolate_multi/gfacs_o/gene_table.txt > final_entap_proteins_filter_ltr_gene_table.txt

genome="/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/3ddna_post/26_chr_length_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.fasta"
alignment="final_entap_proteins_filter_ltr_gene_table.txt"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--statistics-at-every-step \
--splice-table \
--get-protein-fasta \
--fasta "$genome" \
--create-gtf \
--create-gff3 \
-O gfacs_o \
"$alignment"
```

</details>

<details><summary>f. Functionally annotate final gFACs combined LTR filtered monoexonics + multiexonics</summary>

Full script: [entap_braker_rna_only_rerun_post_ltr_filtering.sh](3_Annotation/entap/entap_braker_rna_only_rerun_post_ltr_filtering.sh)

```
module load EnTAP/0.10.8
module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/2.0.6
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0
module load eggnog-mapper/0.99.1

EnTAP --runP --ini entap_config.ini -i $input_protein/genes.fasta.faa -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.208.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --threads 16
```

</details>

</details>

| gFACs Run | Number of Genes | Number of Monoexonic Genes | Number of Multiexonic Genes | BUSCO |
|-|-|-|-|-|
| Initial BRAKER | 49,012 | 18,245 | 30,767 | C:92.1%[S:78.5%,D:13.6%] |
| Post-filtering | 33,105 | 2,338 | 30,767 | C:91.8%[S:78.2%,D:13.6%] |

### EnTAP Stats:
Total Sequences: 33,105  
Similarity Search  
  Total unique sequences with an alignment: 21122
  Total unique sequences without an alignment: 11983
Gene Families  
  Total unique sequences with family assignment: 23910
  Total unique sequences without family assignment: 9195
  Total unique sequences with at least one GO term: 21159
  Total unique sequences with at least one pathway (KEGG) assignment: 7313
Totals  
  Total unique sequences annotated (similarity search alignments only): 1043
  Total unique sequences annotated (gene family assignment only): 3831
  Total unique sequences annotated (gene family and/or similarity search): 24953
  Total unique sequences unannotated (gene family and/or similarity search): 8152
