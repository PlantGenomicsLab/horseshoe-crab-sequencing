#!/bin/bash
#SBATCH --job-name=flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 40
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load flye/2.9

flye --nano-raw /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rmv_contam_24APR19_HsC-circul_combined_reads.fq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rmv_contam_24APR19_HsC-SPRIselect_combined_reads.fq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rerun_rmv_contam_29MAY2019_HsC-male-blood_PAD59173_PRO002_LSK109-reads-pass.fastq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rmv_contam_30APR19_HsC-circul-full_combined_reads.fq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rmv_contam_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21.fastq --genome-size 1.17g --out-dir flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam --threads 40

