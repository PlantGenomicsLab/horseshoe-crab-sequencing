#!/bin/bash
#SBATCH --job-name=centrifuge_limulus_promethion_blood
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load centrifuge/1.0.4-beta

centrifuge -x abfpv -p 16 --report-file centrifuge_limulus_promethion_blood_reads_report.tsv --quiet --min-hitlen 30 -q /archive/labs/wegrzyn/genomes/horseshoe_crab/reads/promethion/29MAY2019_HsC-male-blood_PAD59173_PRO002_LSK109-reads-pass.fastq


