#!/bin/bash
#SBATCH --job-name=centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 40
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=185G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load centrifuge/1.0.4-beta

#cat /archive/projects/EBP/roneill/reads/nanopore/promethion/limulus/2019NOV13_Limulus_malemuscle_PAE06115_LSK109/2019NOV13_Limulus_malemuscle_PAE06115_LSK109/20191113_2151_1-A7-D7_PAE06115_0a8ac8e7/super_basecalled/Limulus_2019NOV13_Super_Accurate_18NOV21/pass/*.fastq /archive/projects/EBP/roneill/reads/nanopore/promethion/limulus/2019Nov15_HSC_LSK109_restart/2019Nov15_HSC_LSK109_restart/20191115_1602_1-A7-D7_PAE06115_db407b7a/super_basecalled/2019NOV15_HSC_LSK109_restart_Super_Accurate_22NOV21/pass/*.fastq > promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21.fastq

centrifuge -x abfpv -p 40 --report-file centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_reads_report.tsv --quiet --min-hitlen 30 -q promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21.fastq





