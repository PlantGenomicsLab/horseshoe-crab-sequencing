#!/bin/bash
#SBATCH --job-name=centrifuge_build_index
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 48
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=985G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load centrifuge/1.0.4-beta
module load blast/2.7.1

centrifuge-download -P 32 -o library -m -d "viral" refseq > seqid2taxid.map
centrifuge-download -P 32 -o library -m -d "archaea" refseq >> seqid2taxid.map
centrifuge-download -P 32 -o library -m -d "protozoa" refseq >> seqid2taxid.map
centrifuge-download -P 32 -o library -m -d "fungi" refseq >> seqid2taxid.map
centrifuge-download -P 32 -o library -m -d "bacteria" refseq >> seqid2taxid.map

cat library/archaea/*.fna > input-sequences.fna
cat library/bacteria/*.fna >> input-sequences.fna
cat library/viral/*.fna >> input-sequences.fna
cat library/fungi/*.fna >> input-sequences.fna
cat library/protozoa/*.fna >> input-sequences.fna

/home/FCAM/astarovoitov/centrifuge_loc/bin/centrifuge-build -p 48 --conversion-table seqid2taxid.map \
--taxonomy-tree taxonomy/nodes.dmp --name-table taxonomy/names.dmp \
input-sequences.fna abfpv
