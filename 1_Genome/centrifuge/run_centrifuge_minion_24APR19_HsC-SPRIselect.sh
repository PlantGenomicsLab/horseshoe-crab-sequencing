#!/bin/bash
#SBATCH --job-name=centrifuge_limulus_minion_24APR19_HsC-SPRIselect
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load centrifuge/1.0.4-beta

centrifuge -x abfpv -p 16 --report-file centrifuge_limulus_minion_24APR19_HsC-SPRIselect_reads_report.tsv --quiet --min-hitlen 30 -q /archive/labs/wegrzyn/genomes/horseshoe_crab/reads/minion/24APR19_HsC-SPRIselect_FAK71862_LSK109_Min106/24APR19_HsC-SPRIselect_FAK71862_LSK109_Min106/20190424_1733_MN17911_FAK71862_63a34bb5/fastq/pass/24APR19_HsC-SPRIselect_combined_reads.fq


