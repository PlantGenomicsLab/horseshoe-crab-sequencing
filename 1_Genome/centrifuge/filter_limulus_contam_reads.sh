#!/bin/bash
#SBATCH --job-name=filter_limulus_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=185G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

#Create Lists of Classified Read Ids with no duplicates from Centrifuge output
for i in minion_30APR19_HsC-circul-full_3627649 minion_24APR19_HsC-SPRIselect_3627609 minion_24APR19_HsC-circul_3627613 promethion_muscle_3627544 promethion_blood_3627513; do
	grep -vw "unclassified" centrifuge_limulus_"$i".out > centrifuge_limulus_"$i"_contaminated_reads.txt
	awk NF=1 centrifuge_limulus_"$i"_contaminated_reads.txt > centrifuge_limulus_"$i"_contaminated_read_ids.txt
	sort -u centrifuge_limulus_"$i"_contaminated_read_ids.txt > no_dup_centrifuge_limulus_"$i"_contaminated_read_ids.txt
done

grep -vw "unclassified" centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_3700319.out > centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_contaminated_reads.txt
awk NF=1 centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_contaminated_reads.txt > centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_contaminated_read_ids.txt
sort -u centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_contaminated_read_ids.txt > no_dup_centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_contaminated_read_ids.txt

#Filter read sets using list of read ids with BBMap
/home/FCAM/astarovoitov/bbmap/filterbyname.sh in=/archive/labs/wegrzyn/genomes/horseshoe_crab/reads/minion/24APR19_HsC-circul_FAK70868_LSK109_Min106/24APR19_HsC-circul_FAK70868_LSK109_Min106/20190424_1909_MN17898_FAK70868_5c9a2451/fastq/pass/24APR19_HsC-circul_combined_reads.fq out=bb_test_rmv_contam_24APR19_HsC-circul_combined_reads.fq names=no_dup_centrifuge_limulus_minion_24APR19_HsC-circul_3627613_contaminated_read_ids.txt include=f
/home/FCAM/astarovoitov/bbmap/filterbyname.sh in=/archive/labs/wegrzyn/genomes/horseshoe_crab/reads/minion/30APR19_HsC-circul-full_FAK71862_LSK109_Min106/fastq/pass/30APR19_HsC-circul-full_combined_reads.fq out=rmv_contam_30APR19_HsC-circul-full_combined_reads.fq names=no_dup_centrifuge_limulus_minion_30APR19_HsC-circul-full_3627649_contaminated_read_ids.txt include=f
/home/FCAM/astarovoitov/bbmap/filterbyname.sh in=/archive/labs/wegrzyn/genomes/horseshoe_crab/reads/minion/24APR19_HsC-SPRIselect_FAK71862_LSK109_Min106/24APR19_HsC-SPRIselect_FAK71862_LSK109_Min106/20190424_1733_MN17911_FAK71862_63a34bb5/fastq/pass/24APR19_HsC-SPRIselect_combined_reads.fq out=rmv_contam_24APR19_HsC-SPRIselect_combined_reads.fq names=no_dup_centrifuge_limulus_minion_24APR19_HsC-SPRIselect_3627609_contaminated_read_ids.txt include=f
/home/FCAM/astarovoitov/bbmap/filterbyname.sh qin=33 in=/archive/labs/wegrzyn/genomes/horseshoe_crab/reads/promethion/29MAY2019_HsC-male-blood_PAD59173_PRO002_LSK109-reads-pass.fastq out=rerun_rmv_contam_29MAY2019_HsC-male-blood_PAD59173_PRO002_LSK109-reads-pass.fastq names=no_dup_centrifuge_limulus_promethion_blood_3627513_contaminated_read_ids.txt include=f
/home/FCAM/astarovoitov/bbmap/filterbyname.sh qin=33 in=/archive/labs/wegrzyn/genomes/horseshoe_crab/reads/promethion/2019NOV13_Limulus_malemuscle_combined_reads-pass.fastq out=rerun_rmv_contam_2019NOV13_Limulus_malemuscle_combined_reads-pass.fastq names=no_dup_centrifuge_limulus_promethion_muscle_3627544_contaminated_read_ids.txt include=f
/home/FCAM/astarovoitov/bbmap/filterbyname.sh qin=33 in=/core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21.fastq out=rmv_contam_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21.fastq names=no_dup_centrifuge_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21_contaminated_read_ids.txt include=f

