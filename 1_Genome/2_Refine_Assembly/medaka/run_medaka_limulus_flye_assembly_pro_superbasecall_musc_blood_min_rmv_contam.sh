#!/bin/bash
#SBATCH --job-name=medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_min_rmv_contam
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=64
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=250G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load medaka/1.5.0
#module load python/3.6.3
#module unload tabix/0.2.6
#module load zlib/1.2.11
#module load vcftools/0.1.16
#export TF_FORCE_GPU_ALLOW_GROWTH=true

#cp /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/flye_assembly_all_reads/flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/assembly.fasta /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/medaka/medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/
#cat /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rmv_contam_24APR19_HsC-circul_combined_reads.fq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rmv_contam_24APR19_HsC-SPRIselect_combined_reads.fq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rerun_rmv_contam_29MAY2019_HsC-male-blood_PAD59173_PRO002_LSK109-reads-pass.fastq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rmv_contam_30APR19_HsC-circul-full_combined_reads.fq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/centrifuge/rmv_contam_promethion_limulus_muscle_2019NOV13_Super_Accurate_18NOV21.fastq > limulus_pro_superbasecall_musc_blood_min_reads_rmv_contam.fastq
medaka_consensus -i limulus_pro_superbasecall_musc_blood_min_reads_rmv_contam.fastq -d assembly.fasta -o medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_min_rmv_contam -t 64 -m r941_prom_high_g303

