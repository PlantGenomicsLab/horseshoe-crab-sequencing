#!/bin/bash
#SBATCH --job-name=hic_clean_reads_juicer_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH -p himem
#SBATCH -q himem
#SBATCH --mail-type=END
#SBATCH --mem=32G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o juicer_%j.out
#SBATCH -e juicer_%j.err

module load java-sdk/1.8.0_92
module load bwa/0.7.17

#python /home/FCAM/astarovoitov/juicedir/juicer/misc/generate_site_positions.py Sau3AI hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/purge_dups/purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/ispb_0/consensus/seqs/consensus.purged.fa

#awk 'BEGIN{OFS="\t"}{print $1, $NF}' hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_Sau3AI.txt > hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.chrom.sizes

bash /home/FCAM/astarovoitov/juicedir/juicer/SLURM/scripts/juicer.sh -g hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam -y hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_Sau3AI.txt -z /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/purge_dups/purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/ispb_0/consensus/seqs/consensus.purged.fa -p hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.chrom.sizes


