#!/bin/bash
#SBATCH --job-name=bwa_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 36
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o bwa_%j.out
#SBATCH -e bwa_%j.err

module load bwa/0.7.17
module load samtools/1.7
module load samblaster/0.1.24

#bwa index /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/purge_dups/purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/ispb_0/consensus/seqs/consensus.purged.fa
bwa mem -5SP -t 36 /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/purge_dups/purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/ispb_0/consensus/seqs/consensus.purged.fa /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/fastq/limulus_polyphemus_S3HiC_bpv_unclassified_R1.fastq /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/fastq/limulus_polyphemus_S3HiC_bpv_unclassified_R2.fastq | samblaster | samtools view -S -h -b -F 2316 > limulus_polyphemus_S3HiC_clean_reads_align_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_samblaster.bam


