#!/bin/bash
#SBATCH --job-name=3ddna_hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam_splitter_coarse_resolution_250000
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=64G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o 3ddna_%j.out
#SBATCH -e 3ddna_%j.err

hostname
echo "\nStart time:"
date

export TMPDIR="hic_tmp"

bash /home/FCAM/astarovoitov/3d-dna/run-asm-pipeline.sh --editor-repeat-coverage 4 --splitter-coarse-resolution 250000 /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/purge_dups/purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/ispb_0/consensus/seqs/consensus.purged.fa /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/aligned/merged_nodups.txt

echo "\nEnd time:"
date

