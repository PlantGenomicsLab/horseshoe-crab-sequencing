#!/bin/bash
#SBATCH --job-name=3dpost
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 9
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=72G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
echo "\nStart time:"
date

export TMPDIR="hic_tmp"

bash /home/FCAM/astarovoitov/3d-dna/run-asm-pipeline-post-review.sh -r 26_chr_consensus.purged.final.review.assembly /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/purge_dups/purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/ispb_0/consensus/seqs/consensus.purged.fa /core/projects/EBP/Oneill/limulus_combined_promethion_minion_data_assembly/hic/hic_clean_reads_purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam/aligned/merged_nodups.txt

echo "\nEnd time:"
date



