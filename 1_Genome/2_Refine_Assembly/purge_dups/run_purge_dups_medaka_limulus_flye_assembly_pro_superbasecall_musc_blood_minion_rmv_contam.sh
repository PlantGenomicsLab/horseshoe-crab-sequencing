#!/bin/bash
#SBATCH --job-name=purge_dups_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=32G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.22
module load python/3.6.3
module load purge_dups/1.0.0

run_purge_dups.py -p bash config_medaka_limulus_flye_assembly_pro_superbasecall_musc_blood_minion_rmv_contam.json /isg/shared/apps/purge_dups/1.0.0/src/ purge_dup
